<?php
require("includes/common.php");

$header["title"] = "FashionSales.CH - Dein Online Outlet Shop für Marken-Mode";

$header["meta"]["description"] = "Bei FashionSales.ch finden Marken Mode und Schuhe bis zu 80% reduziert. Desigual, Adidas, Bench, Jack Wolfskin und viele mehr.";


$page = intval(Input::get("page","1"));
if($page>1) {
  $ROBOTS_TAG = "noindex, follow";
}

require("html/header.php");

require("html/menu.php");

require("html/searchform-home.php");

require("html/home-brands.php");

require("html/home-categories.php");

require("html/banner.php");
$offset = ($page-1) * 40;
$sql = 'SELECT SQL_CALC_FOUND_ROWS id, COUNT(id) AS numMerchants,MIN(price) as minPrice, price_old, color, size, gender, shipping_time, name,normalised_name,image_url,description,rating, ROUND( (MIN(price)*100/price_old) ) as percent
    FROM `pt_products`
    GROUP BY id
    ORDER BY percent ASC, RAND()
    LIMIT '.$offset.', 40';


$numRows = database_querySelect($sql,$rows);

$searchresults["products"] = $rows;
$searchresults["numRows"] = $numRows;

$sqlResultCount = "SELECT FOUND_ROWS() as resultcount";
database_querySelect($sqlResultCount,$rowsResultCount);
$resultCount = $rowsResultCount[0]["resultcount"];
$navigation["resultCount"] = $resultCount;

foreach($searchresults["products"] as $k => $product)
{
    $searchresults["products"][$k]["productHREF"] = tapestry_productHREF($searchresults["products"][$k]);
}
require("html/searchresults.php");
$config_resultsPerPage = 40;
require("html/navigation.php");
require("html/footer.php");
?>
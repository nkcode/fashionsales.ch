$.widget( "custom.catcomplete", $.ui.autocomplete, {
    _create: function() {
      this._super();
      this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
    },
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        var li;
        if ( item.category != currentCategory ) {
          var label;

          switch(item.category) {
            case "brand":
              label = "Marken Shop"
              break;
            case "product":
              label = "Produkte";
              break;
            case "category":
              label = "Kategorie Shop";
              break;
          }
          console.log(label)
          ul.append( "<li class='ui-autocomplete-category "+item.category+"'>" + label + "</li>" );
          currentCategory = item.category;
        }
        li = that._renderItemData( ul, item );
        li.addClass(item.category)
        li.html("<div class=ui-autocomplete-img-label>"+li.html()+"</div>")
        if(item.image) {
          li.prepend("<img src='"+item.image+"'>");
        }
        if ( item.category ) {
          li.attr( "aria-label", item.category + " : " + item.label );
        }
      });
    }
  });

$(function() {
  $("input[name=q]")
    .catcomplete(
      {
        source: function( request, response ) {
          $.getJSON( "/search.php", {
            q: request.term,
            for_autocomplete: 'true'
          }, response );
        },
        select: function(e,ui) {
          e.preventDefault()
          $("input[name=q]").val(ui.item.value)
          $("form[name='search']").submit()
        },
        open: function() {
          var max_width = 0;
          $(".ui-menu-item.brand img").load(function() {

            //vertical align
            $(this).css("margin-top",((38-$(this).height())/2)+"px")

            var all_loaded = true;

            $(".ui-menu-item.brand img").each(function() {
              if($(this).height()==0 || $(this).width()==0) {
                all_loaded = false;
              }
            })

            if(!all_loaded) return;

            $(".ui-menu-item.brand img").each(function() {
              if($(this).width()>max_width) {
                max_width = $(this).width()
              }
            })


            $(".ui-menu-item.brand .ui-autocomplete-img-label").css("margin-left",(max_width+20)+"px")

            //images center align
            $(".ui-menu-item.brand img").each(function() {
              $(this).css("margin-left", ((max_width-$(this).width())/2)+"px")
            })

          })

          var max_width_product = 0;

          $(".ui-menu-item.product img").load(function() {
            if($(this).width()>max_width) {
              max_width_product = $(this).width()
              $(".ui-menu-item.product .ui-autocomplete-img-label").css("margin-left",(max_width_product+20)+"px")
            }
          })

        }
    }
  )

 $('.ui-autocomplete').addClass('f-dropdown medium');    

})

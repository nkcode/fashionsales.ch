<?
  if($_GET['for_autocomplete']) {
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");    

    $ret = array();

    //brands
    $rows = Product::select(array("brand","brand_logo"))
      ->where("brand","like","%".$_GET['q']."%")
      ->whereRaw("TRIM(IFNULL(brand_logo, ''))>''")
      ->groupBy("brand")->limit(5)->get();


    foreach($rows as $row) {
        array_push($ret, array("value"=>"brand:".strtolower($row["attributes"]["brand"]).":", 
          "image"=>$row["attributes"]["brand_logo"],
          "label"=>$row["attributes"]["brand"], 
          "category"=>"brand") );
    }

    //categories
    $sql = "SELECT DISTINCT category_name FROM `".$config_databaseTablePrefix."newcategories` WHERE category_name LIKE \"%".database_safe($_GET['q'])."%\" ORDER BY category_name LIMIT 5";

    if (database_querySelect($sql,$rows)) {
      foreach($rows as $category) {
        array_push($ret, array("value"=>"category:".strtolower($category["category_name"]).":",
         "label"=>$category["category_name"], 
         "category"=>"category") );
      }
    }

    //products
    if(isset($searchresults["products"])) {
      foreach($searchresults["products"] as $product) {

        array_push($ret,array(
          "value"=>$product["name"],
          "label"=>$product["name"],
          "image"=>$product["image_url"],
          "category"=>"product"));
        if(count($ret)>=10) {
          break;
        }
      }
    }



    echo json_encode($ret);
    die();
  }
?>
<?php
  require("includes/common.php");

  $header["title"] = "Example Page";

  $header["meta"]["description"] = "Example page meta description";

  $header["meta"]["keywords"] = "Example, Page, Meta, Keywords";

  require("html/header.php");
  require("html/menu.php");

  require("html/searchform.php");
?>

<div class='row'>
<p>Die Inhalte der Website FashionSales unterliegen &ndash; sofern nicht anders gekennzeichnet &ndash; dem Urheberrecht und dürfen nicht ohne vorherige schriftliche Zustimmung von FashionSales weder als Ganzes noch in Teilen verbreitet, verändert oder kopiert werden. Die auf dieser Website eingebundenen Bilder dürfen nicht ohne vorherige schriftliche Zustimmung von FashionSales verwendet werden. Auf den Websites enthaltene Bilder unterliegen teilweise dem Urheberrecht Dritter.</p>
<h1>Haftungsausschluss</h1>

<p>Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluß zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind!</p>


<h1>Impressum</h1>
<p></p><div class="clear"></div>
<div><p>Jelena Mihajlovic</p><p>Bärematt 1</p><p>6017 Ruswil</p><p>Schweiz</p><p>Tel: +41 76 77 99 820</p></div>
</div>

<?php
  require("html/footer.php");
?>
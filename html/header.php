<?php
  if (file_exists("html/user_header_before.php")) require("html/user_header_before.php");

  header("Content-Type: text/html;charset=".$config_charset);
?>
<!DOCTYPE HTML>

<html>

  <head>

    <meta name='viewport' content='width=device-width, initial-scale=1.0' />


    <? require "html/header.category_tags.php" ?>
    <? require "html/header.brand_tags.php" ?>

    <title><?php print (isset($header["title"])?htmlspecialchars($header["title"],ENT_QUOTES,$config_charset):$config_title); ?></title>

      <?php if (isset($header["meta"])): foreach($header["meta"] as $name => $content): ?>

        <meta name='<?php print $name; ?>' content='<?php print htmlspecialchars($content,ENT_QUOTES,$config_charset); ?>' />

      <?php endforeach; endif; ?>

	     <meta name="robots" content="<?= $ROBOTS_TAG ? $ROBOTS_TAG : "index,follow" ?>">

      <link rel='stylesheet' href='<?php print $config_baseHREF; ?>html/vendor/foundation.min.css' />

      <link rel='stylesheet' href='<?php print $config_baseHREF; ?>html/default.css?<?= filemtime("html/default.css") ?>' />


      <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <? if(!$DEVELOPMENT_ENV): ?>
  	  <script>
  	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  	  ga('create', 'UA-56490142-1', 'auto');
  	  ga('require', 'displayfeatures');
  	  ga('set', 'anonymizeIp', true);
  	  ga('require', 'linkid', 'linkid.js');

  	  ga('send', 'pageview');

  	  </script>
    <? endif; ?>
	  <!-- TradeDoubler site verification 2456297 -->
    </head>

  <body>

  <?php
    if (file_exists("html/user_header_after.php")) require("html/user_header_after.php");
  ?>
  
<link rel="stylesheet" href="/bower_components/jquery-ui/themes/base/core.css" />
<link rel="stylesheet" href="/bower_components/jquery-ui/themes/base/autocomplete.css" />
<link rel="stylesheet" href="/bower_components/jquery-ui/themes/base/menu.css" />
<link rel="stylesheet" href="/bower_components/jquery-ui/themes/base/slider.css" />
<link rel="stylesheet" href="/bower_components/jquery-ui/themes/base/theme.css" />
<script src='/bower_components/jquery-ui/jquery-ui.min.js'></script>
<script src="/js/search.autocomplete.js"></script>

<style>
  .ui-helper-hidden-accessible{display:none}
  .ui-autocomplete-loading {
    background: white url("/images/ui-anim_basic_16x16.gif") right 5px center no-repeat !important;
  }  
  .ui-autocomplete-category {
    font-weight: bold;
    padding: .2em .4em;
    margin: .8em 0 .2em;
    line-height: 1.5;
  }  
  .ui-menu-item {
    padding-left: 20px !important;
  }
  .ui-menu-item img {
    max-height: 40px;
    margin-right: 10px;
    max-width: 200px;
    vertical-align: middle !imporant;
    position: absolute;
  }
  .ui-menu-item .ui-autocomplete-img-label {
    position: absolute;
    display: inline-block;
    margin-left: 50px;
    height: 40px;
    padding-top: 10px;
  }

  .ui-menu-item .ui-autocomplete-img-label {
    position: relative;
    display: inline-block;
    height: 40px;
    padding-top: 10px;
  }

  .ui-menu-item.category .ui-autocomplete-img-label {
    margin-left: 0px;
    height: 20px;
    padding-top: 0px;
  }

  .ui-autocomplete-category, .ui-menu-item {
    background: transparent !important;
  }

  .ui-menu-item{
    opacity: 0.8 !important;
  }

  .ui-menu-item:hover{
    opacity: 1 !important;
  }

  .ui-autocomplete-category.category, .ui-autocomplete-category.product {
    padding-top: 20px !important;
  }


</style>
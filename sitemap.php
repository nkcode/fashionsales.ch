<?php
  require("includes/common.php");

  header("Content-Type: text/xml");

  print "<?xml version='1.0' encoding='UTF-8'?>";

  $limit = 50000;

  if (isset($_GET["filename"]))
  {
    $start = (isset($_GET["start"])?intval($_GET["start"]):0);

    print "<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";

    $sql = "SELECT normalised_name FROM `".$config_databaseTablePrefix."products` WHERE filename='".database_safe($_GET["filename"])."' LIMIT ".$start.",".$limit;

    $link = mysqli_connect($config_databaseServer,$config_databaseUsername,$config_databasePassword,$config_databaseName);

    mysqli_real_query($link,$sql);

    $result = mysqli_use_result($link);

    $sitemapBaseHREF = "http://".$_SERVER["HTTP_HOST"];

    while($row = mysqli_fetch_assoc($result))
    {
      print "<url>";

      $sitemapHREF = tapestry_productHREF($row);

      print "<loc>".$sitemapBaseHREF.$sitemapHREF."</loc>";

      print "</url>";
    }

    print "</urlset>";
  }
  else
  {
    print "<sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>";

    $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds` WHERE imported > 0 ORDER BY filename";

    if (database_querySelect($sql,$rows))
    {
      $sitemapBaseHREF = "http://".$_SERVER["HTTP_HOST"].$config_baseHREF;

      foreach($rows as $row)
      {
        $start = 0;

        while($start < $row["products"])
        {
          print "<sitemap>";

          $sitemapHREF = "sitemap.php?filename=".urlencode($row["filename"]);

          if ($start)
          {
            $sitemapHREF .= "&amp;start=".$start;
          }

          print "<loc>".$sitemapBaseHREF.$sitemapHREF."</loc>";

          print "<lastmod>".date("Y-m-d",$row["imported"])."</lastmod>";

          print "</sitemap>";

          $start += $limit;
        }
      }
    }

    print "</sitemapindex>";
  }
?>
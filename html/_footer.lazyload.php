	  <script src='<?= $config_baseHREF ?>bower_components/jquery.lazyload/jquery.lazyload.min.js'></script>
	  <script>
	  $(function() {
	  	$(".pt_sr_image, .item-logo").each(function() {
	  		var src = $(this).attr("src")
	  		$(this).removeAttr("src")
	  		$(this).attr("data-original",src)
	  	})
	  	$(".pt_sr_image, .item-logo").lazyload({
	  		 threshold : 200
	  	})
	  })
	  </script>

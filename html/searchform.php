<div class="row top-area">

<div class="medium-4 columns">

<? require "html/search.shop_title.php"; ?>

</div>

<div class="medium-8 columns">

<div class='pt_sf search_form'>

  <div class='row'>

    <div class='search_form_inner medium-12 <?php print (isset($searchform["centered"])?"medium-centered":""); ?> columns'>

      <form name='search' action='<?php print $config_baseHREF ?>search.php'>

          <div class="row collapse">

            <div class="small-10 columns">

              <input required="required" type="text" name="q" value="<?php /* print (isset($q)?$q:""); */ ?>">

            </div>

            <div class="small-2 columns">

              <button class="button tiny postfix"><?php print translate("Search"); ?></button>

            </div>

          </div>

      </form>


    </div>
	
</div>
	
</div>



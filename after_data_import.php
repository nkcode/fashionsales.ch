<?

	header( 'Expires: Sat, 26 Jul 1997 05:00:00 GMT' ); 
	header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' ); 
	header( 'Cache-Control: no-store, no-cache, must-revalidate' ); 
	header( 'Cache-Control: post-check=0, pre-check=0', false ); 
	header( 'Pragma: no-cache' ); 
	header( 'Content-type: text/html; charset=utf-8' );

	require("includes/common.php");
	set_time_limit(60*10);

	if(Input::get("products_table_backup")) {
		echo "backing up products<br>";flush();ob_flush();
		DB::statement("drop table if exists pt_products_backup");
		DB::statement("create table pt_products_backup like pt_products");
		DB::statement("insert pt_products_backup select * from pt_products;");
		die("done");
	} else
	if(Input::get("products_table_restore")) {
		echo "restoring products<br>";flush();ob_flush();
		DB::statement("drop table if exists pt_products;");
		DB::statement("create table pt_products like pt_products_backup;");
		DB::statement("insert pt_products select * from pt_products_backup;");
		die("done");
	} else {
		echo_ln("fixing corrupted german letters");
		echo_ln("IMPORTANT: run this script ONLY ONCE after data import (runing it second time will convert all german letters to latin)");


		Product::field_fix_after_import("normalised_name",255);
		Product::field_fix_after_import("merchant",32);
		Product::field_fix_after_import("brand",32);
		Product::field_fix_after_import("color",64);
		Product::field_fix_after_import("size",64);
		Product::field_fix_after_import("gender",64);

		echo "removing name index<br>";flush();ob_flush();
		DB::statement("ALTER TABLE `pt_products` DROP KEY `name_2`;");
		Product::field_fix_after_import("name",255);
		echo "restoring name index<br>";flush();ob_flush();
    DB::statement("ALTER TABLE `pt_products` ADD FULLTEXT KEY `name_2` (`name`);");

		Product::field_fix_after_import("category",64);

		echo_ln("DONE. NOW YOU SHOULD SEE RIGHT GERMAN LETTERS. ");


		ProductSize::truncate();
		echo_ln("BUILDING PRODUCT SIZES");
		echo_ln("all product sizes deleted");


		global $total, $count;
		$count = 0;
		$total = Product::count();
		DB::disableQueryLog();
		Product::chunk(1000, function ($products) {
			global $count;
			global $total;
			$count += 1000;
			$percent = round($count*100/$total);
			echo $percent."% ".$count."/".$total."<br>";
			flush();ob_flush();

			foreach($products as $product) {
				$product->build_sizes();
			}
		});	

		echo "EVERYTHING DONE";

	}




?>
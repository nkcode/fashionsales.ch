<div class="row tops">
    <? if(isset($TOP_CATEGORIES) && !$_GET['catalog_by_category']) {
        $list = array();
             foreach($TOP_CATEGORIES as $category=>$count) {
                 $list[] = '<span class="top category medium-2 small-12 columns">
                    <a href="/b/'.str_replace(" ","-",$_GET['catalog_by_brand']).'/'.
                     str_replace(" ","-",strtolower($category)).
                     ($_GET['catalog_by_category']? '/'.str_replace(" ","-",$_GET['catalog_by_category']): '').
                     '">'.$category.' ('.$count.')</a>
                </span>';
             }
        echo implode($list);
        ?>
    <? } ?>

    <? if(isset($TOP_BRANDS)) { 
        $category = Input::get("catalog_by_category"); ?>
        <h2 class=top_brands><?= ucfirst($category) ?> weitere Marken</h2>
        <? foreach($TOP_BRANDS as $brand=>$count) { ?>
            <span class="top brand medium-2 small-12 columns">
                <a href="/b/<?= name_for_url($brand) ?>/<?= name_for_url($category) ?>" >
                    <?= $brand ?>
                    (<?= $count ?>)
                </a>
            </span>
        <? } ?>
    <? } ?>
</div>

<div class="under_filter_wrapper row">

<?= (isset($banner["under_filter"])?"<h2 class=under_filter>".$banner["under_filter"]."</h2>":"");  ?>
<?= (isset($banner["under_filter_2"])?"<h2 class=under_filter_2>".$banner["under_filter_2"]."</h2>":"");  ?>
<?= (isset($banner["p_under_filter"])?"<p class=under_filter>".$banner["p_under_filter"]."</p>":""); ?>
</div>

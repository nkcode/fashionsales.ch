<?php
  $translate["More Information"] = "mehr Informationen";
  $translate["Order by"] = "Sortieren nach";
  $translate["Relevance"] = "SALE";
  $translate["Best Price"] = "Preis";
  $translate["brand"] = "Brands";
  $translate["Brand"] = "Mode Brands";
  $translate["Catalogue Product Name"] = "Artikelname";
  $translate["Category"] = "Mode Kategorien";
  $translate["category"] = "Kategorien";
  $translate["Comments"] = "Kommentare";
  $translate["Compare Prices"] = "Vergleichen Sie Preise";
  $translate["Featured Products"] = "Hervorgehobene Produkte";
  $translate["from"] = "bei";
  $translate["go to"] = "gehen Sie zu";
  $translate["High to Low"] = "absteigend";
  $translate["Home"] = "Startseite";
  $translate["Low to High"] = "aufsteigend";
  $translate["Merchant"] = "Shop";
  $translate["merchant"] = "Shop";
  $translate["Next"] = "weiter";
  $translate["no results found"] = "keine Ergebnisse gefunden";
  $translate["of"] = "von";
  $translate["Previous"] = "Vorherige";
  $translate["Price"] = "Preis";
  $translate["Price search results for"] = "Ergebnisse f&uuml;r Ihre Preissuche nach";
  $translate["product not found"] = "Produkt nicht gefunden";
  $translate["Product Rating"] = "Produktbewertung";
  $translate["Product reviews for"] = "Produktberichte f&uuml;r";
  $translate["Product search results for"] = "Ergebnisse f&uuml;r Ihre Produktsuche nach";
  $translate["Rating"] = "Bewertung";
  $translate["Review This Product"] = "Produkt bewerten";
  $translate["Reviews"] = "Berichte";
  $translate["Search"] = "Suche";
  $translate["Search or browse by"] = "Suchen oder st&ouml;bern Sie nach";
  $translate["Show All Prices"] = "Alle Preise anzeigen";
  $translate["showing"] = "Suchergebnisse von";
  $translate["Stockist"] = "Shop";
  $translate["Submit"] = "Senden";
  $translate["This reviewer did not leave any comments."] = "Kein Kommentar verf&uuml;gbar";
  $translate["Filter These Results"] = "Suche verfeinern";
  $translate["to"]  = "bis";
  $translate["or"] = "oder";
  $translate["Visit Store"] = "Shop besuchen";
  $translate["Your Comments (optional)"] = "Ihre Kommentare (optional)";
  $translate["Your Rating"] = "Ihre Bewertung";
  $translate["Your review is pending approval. Thank you for your contribution."] = "Deine Bewertung wird &uuml;berpr&uuml;ft. Danke!";
  $translate["Your review will appear on the website after it has been approved.  Thank you for your contribution."] = "Deine Bewertung wird &uuml;berpr&uum;ft. Danke!";

  $translate["Color"] = "Farbe";
  $translate["color"] = "Farbe";
  $translate["Size"] = "Grösse";
  $translate["size"] = "Grösse";
  $translate["Gender"] = "Für";
  $translate["gender"] = "Für:";
  $translate["ShippingTime"] = "Lieferung";
function translate($text)
  {
    global $translate;
    return (isset($translate[$text])?$translate[$text]:$text);
  }
?>
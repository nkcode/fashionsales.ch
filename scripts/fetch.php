<?php
  set_time_limit(0);

  require("../includes/common.php");

  require("../includes/admin.php");

  require("../includes/automation.php");

  $argc = $_SERVER["argc"];

  $argv = $_SERVER["argv"];

  if ($argc < 2)
  {
    print "Usage: fetch.php <filename>|@ALL\n"; exit;
  }

  $filename = $argv[1];

  function fetch()
  {
    global $job;

    print chr(13)."fetching ".$job["filename"];

    $status = automation_run($job["id"]);

    print chr(13)."fetching ".$job["filename"]."...[".$status."]            \n";
  }

  if ($filename == "@ALL")
  {
    $sql = "SELECT * FROM `".$config_databaseTablePrefix."jobs` ORDER BY filename";
  }
  else
  {
    $sql = "SELECT * FROM `".$config_databaseTablePrefix."jobs` WHERE filename='".database_safe($filename)."'";
  }

  if (database_querySelect($sql,$jobs))
  {
    foreach($jobs as $job)
    {
      fetch();
    }
  }

  exit();
?>
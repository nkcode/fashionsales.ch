<?php
  require("includes/common.php");

  $atoz["items"] = array();

  $SQL_FIELDS = "brand, max(IFNULL(brand_logo,'')) as brand_logo, count(1) as count";

  $rows = Product::selectRaw($SQL_FIELDS)->groupBy("brand")->orderBy("brand")->get()->toArray();

  if (count($rows)>0)
  {
    foreach($rows as $product)
    {
      if ($product["brand"])
      {
        $item = array();

        $item["name"] = $product["brand"];
        $item["logo"] = $product["brand_logo"];
        $item["count"] = $product["count"];

        if ($config_useRewrite)
        {
          $item["href"] = strtolower(urlencode(tapestry_hyphenate($product["brand"]))."/");
        }
        else
        {
          $item["href"] = strtolower("search.php?q=brand:".urlencode($product["brand"]).":");
        }

        $atoz["items"][] = $item;
      }
    }
  }

  $header["title"] = translate("Brand")." A-Z";

  $banner["h2"] = "<strong>".translate("Brand")." A-Z</strong>";

  $atoz_class = "brands";

  require("html/header.php");

  require("html/menu.php");

  require("html/searchform-home.php");

  require("html/banner.php");

  require("html/atoz.php");

  require("html/footer.php");
?>
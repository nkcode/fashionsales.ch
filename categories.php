<?php
  require("includes/common.php");

  $atoz["items"] = array();

  $SQL_FIELDS = "category as category_name, count(1) as count";

  //$sql = "SELECT {$SQL_FIELDS} FROM `".$config_databaseTablePrefix."products` GROUP BY category_name ORDER BY category_name";

  $rows = Product::selectRaw($SQL_FIELDS)->groupBy("category_name")->orderBy("category_name")->get()->toArray();

  if (count($rows)>0)
  {
    foreach($rows as $category)
    {
      if ($category["category_name"])
      {
        $item = array();

        $item["name"] = $category["category_name"];
        $item["count"] = $category["count"];

        if ($config_useRewrite)
        {
          $item["href"] = strtolower(urlencode(tapestry_hyphenate($category["category_name"]))."/");
        }
        else
        {
          $item["href"] = strtolower("search.php?q=category:".urlencode($category["category_name"]).":");
        }

        $atoz["items"][] = $item;
      }
    }
  }

  $header["title"] = translate("Category")." A-Z";

  $banner["h2"] = "<strong>".translate("Category")." A-Z</strong>";

  require("html/header.php");

  require("html/menu.php");

  require("html/searchform-home.php");

  require("html/banner.php");

  $atoz_class = "categories";
  require("html/atoz.php");

  require("html/footer.php");
?>
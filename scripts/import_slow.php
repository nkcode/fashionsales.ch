<?php
  require("../includes/common.php");

  $password = (isset($_GET["password"])?$_GET["password"]:"");

  if ($password != $config_adminPassword)
  {
    header('HTTP/1.0 401 Unauthorized');

    print "<h1>401 Unauthorized</h1>";

    exit();
  }  
  
  require("../includes/admin.php");

  require("../includes/filter.php");

  require("../includes/MagicParser.php");

  function import_slow_pre()
  {
    global $config_databaseTablePrefix;

    global $admin_importFeed;

    $sql = "DELETE FROM `".$config_databaseTablePrefix."products` WHERE filename='".database_safe($admin_importFeed["filename"])."'";

    database_queryModify($sql,$insertId);
  }

  function import_slow_each()
  {
    global $admin_importLimit;

    global $admin_importCallback;

    $admin_importLimit = 0;

    $admin_importCallback = FALSE;

    admin_importSetGlobals();
  }

  function import_slow_post()
  {
    global $config_databaseTablePrefix;

    global $admin_importFeed;

    $sql = "SELECT COUNT(*) AS productCount FROM `".$config_databaseTablePrefix."products` WHERE filename='".database_safe($admin_importFeed["filename"])."'";

    database_querySelect($sql,$rows);

    $productCount = $rows[0]["productCount"];

    $sql = "UPDATE `".$config_databaseTablePrefix."feeds` SET imported='".time()."',products='".$productCount."' WHERE filename='".database_safe($admin_importFeed["filename"])."'";

    database_queryModify($sql,$insertId);
  }

  function slow__importRecordHandler($record)
  {
    global $config_slowImportBlock;

    global $progress;

    global $count;

    global $newoffset;

    $progress++;

    $count++;

    admin__importRecordHandler($record);

    if ($count==$config_slowImportBlock)
    {
      $newoffset = MagicParser_getOffsetData();

      return TRUE;
    }
  }

  if (isset($_GET["filename"]))
  {
    $startTime = (isset($_GET["startTime"])?$_GET["startTime"]:time());

    $progress = (isset($_GET["progress"])?$_GET["progress"]:0);

    $offset = (isset($_GET["offset"])?$_GET["offset"]:"0|0");

    $count = 0;

    if ($_GET["filename"]=="@ALL")
    {
      $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds` WHERE imported < '".$startTime."' LIMIT 1";
    }
    else
    {
      $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds` WHERE filename = '".database_safe($_GET["filename"])."'";
    }

    if (!database_querySelect($sql,$rows)) exit();

    $admin_importFeed = $rows[0];

    if ($offset=="0|0")
    {
      import_slow_pre();
    }

    import_slow_each();

    MagicParser_parse($config_feedDirectory.$admin_importFeed["filename"],"slow__importRecordHandler",$admin_importFeed["format"],$offset);

    $refresh = "";

    $refreshBase = $config_baseHREF."scripts/";

    if (MagicParser_completed())
    {
      import_slow_post();

      $more = FALSE;

      if ($_GET["filename"] == "@ALL")
      {
        $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds` WHERE imported < '".$startTime."' LIMIT 1";

        $more = database_querySelect($sql,$result);
      }

      if ($more)
      {
        $refresh = $refreshBase."import_slow.php?filename=".$_GET["filename"]."&progress=0&offset=0|0&startTime=".$startTime;
      }
    }
    else
    {
      $refresh = $refreshBase."import_slow.php?filename=".$_GET["filename"]."&progress=".$progress."&offset=".$newoffset."&startTime=".$startTime;
    }

    if ($refresh)
    {
      if ($config_adminPassword)
      {
        $refresh .= "&password=".$config_adminPassword;
      }

      sleep($config_slowImportSleep);

      header("Location: ".$refresh);
    }
    else
    {
      admin_importReviews();

      print "<p>Done.</p>";
    }

    exit();
  }
  else
  {
    print "<p>Usage: http://www.example.com/scripts/import_slow.php?filename=[filename|@ALL]</p>";
  }
?>
<?php
  set_time_limit(0);

  require("../includes/common.php");

  require("../includes/admin.php");

  require("../includes/filter.php");

  require("../includes/MagicParser.php");

  $argc = $_SERVER["argc"];

  $argv = $_SERVER["argv"];

  if ($argc < 2)
  {
    print "Usage: import.php <filename>|@ALL|@MODIFIED [limit]\n"; exit;
  }

  $filename = $argv[1];

  if (isset($argv[2]))
  {
    $limit = intval($argv[2]);
  }
  else
  {
    $limit = 0;
  }

  function callback($progress)
  {
    global $feed;

    print chr(13)."importing ".$feed["filename"]."...[".$progress."/".$feed["products"]."]";
  }

  function import()
  {
    global $feed;

    global $limit;

    print chr(13)."importing ".$feed["filename"]."...[0/".$feed["products"]."]";

    admin_import($feed["filename"],$limit,"callback");

    print chr(13)."importing ".$feed["filename"]."...[done]            \n";
  }

  if ($filename == "@ALL")
  {
    $admin_importAll = TRUE;

    $sql = "DROP TABLE IF EXISTS `".$config_databaseTablePrefix."products_import`";

    database_queryModify($sql,$result);

    $sql = "CREATE TABLE `".$config_databaseTablePrefix."products_import` LIKE `".$config_databaseTablePrefix."products`";

    database_queryModify($sql,$result);

    $sql = "ALTER TABLE `".$config_databaseTablePrefix."products_import` DISABLE KEYS";

    database_queryModify($sql,$result);

    $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds`";

    if (database_querySelect($sql,$rows))
    {
      foreach($rows as $feed)
      {
        if (file_exists($config_feedDirectory.$feed["filename"]))
        {
          import();
        }
      }
    }
    $sql = "ALTER TABLE `".$config_databaseTablePrefix."products_import` ENABLE KEYS";

    database_queryModify($sql,$result);

    $sql = "DROP TABLE `".$config_databaseTablePrefix."products`";

    database_queryModify($sql,$result);

    $sql = "RENAME TABLE `".$config_databaseTablePrefix."products_import` TO `".$config_databaseTablePrefix."products`";

    database_queryModify($sql,$result);
  }
  elseif($filename == "@MODIFIED")
  {
    $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds`";

    if (database_querySelect($sql,$rows))
    {
      foreach($rows as $feed)
      {
        if (file_exists($config_feedDirectory.$feed["filename"]) && ($feed["imported"] < filemtime($config_feedDirectory.$feed["filename"])))
        {
          import();
        }
      }
    }
  }
  else
  {
    $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds` WHERE filename='".database_safe($filename)."'";

    if (database_querySelect($sql,$rows))
    {
      $feed = $rows[0];

      import();
    }
  }

  print chr(13)."backfilling reviews...                ";

  admin_importReviews();

  print chr(13)."backfilling reviews...[done]\n";

  exit();
?>
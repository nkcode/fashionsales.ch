<?php
  set_time_limit(0);

  require("../includes/common.php");

  require("../includes/admin.php");

  require("../includes/MagicParser.php");

  $argc = $_SERVER["argc"];

  $argv = $_SERVER["argv"];

  if ($argc < 14)
  {
    print "Usage: register.php <filename> <format> <merchant> <fieldMerchant> <fieldName> <fieldDescription> <fieldImageURL> <fieldBuyURL> <fieldPrice> <fieldCategory> <userCategory> <fieldBrand> <userBrand> [<custom1> ... <customN>]\n"; exit;
  }

  $filename = $argv[1];

  $format = $argv[2];

  $merchant = $argv[3];

  $fieldMerchant = $argv[4];

  $fieldName = $argv[5];

  $fieldDescription = $argv[6];

  $fieldImageURL = $argv[7];

  $fieldBuyURL = $argv[8];

  $fieldPrice = $argv[9];

  $fieldCategory = $argv[10];

  $userCategory = $argv[11];

  $fieldBrand = $argv[12];

  $userBrand = $argv[13];

  $registerFields = array();
  $registerFields["name"] = $fieldName;
  $registerFields["description"] = $fieldDescription;
  $registerFields["image_url"] = $fieldImageURL;
  $registerFields["buy_url"] = $fieldBuyURL;
  $registerFields["price"] = $fieldPrice;
  $registerFields["category"] = $fieldCategory;
  $registerFields["brand"] = $fieldBrand;

  $i = 0;

  $j = 14;

  foreach($config_fieldSet as $k => $v)
  {
    $i++;

    if ($i <= 7) continue;

    $registerFields[$k] = $argv[$j++];
  }

  $result = admin_register(
      $filename,
      $format,
      $merchant,
      $fieldMerchant,
      $registerFields,
      $userCategory,
      $userBrand
    );

  print $result."\n";

  exit();
?>
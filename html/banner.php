<?php
  if (strpos($_SERVER["PHP_SELF"],"search.php"))
  {
    if (isset($banner["h2"])) $banner["h2"] = str_replace(translate("Product search results for"),translate("Product search results for")."<br class='show-for-small-only' />",$banner["h2"]);

    if (isset($banner["h3"])) $banner["h3"] = str_replace(" | ".translate("Price")," <br class='show-for-small-only' />".translate("Price"),$banner["h3"]);
  }
?>

<div class='pt_ba row'>

  <div class='small-12 columns'>

    <hr />

    <?php print (isset($banner["h2"])?"<h2>".$banner["h2"]."</h2>":"");  ?>

    <?php print (isset($banner["h3"])?"<h3>".$banner["h3"]."</h3>":"");  ?>

  </div>

</div>
<?php
  require("../includes/common.php");

  require("../includes/widget.php");

  $admin_checkPassword = TRUE;

  require("../includes/admin.php");

  if (isset($_POST["submit"]) && isset($_POST["name"]))
  {
    widget_required("name");

    if (!widget_errorCount())
    {
      $_POST["name"] = trim($_POST["name"]);

      if(!preg_match("/^[0-9a-zA-Z\. ]{0,255}$/",widget_posted($_POST["name"])))
      {
        widget_errorSet("name","category name contains invalid characters");
      }
    }

    if (!widget_errorCount())
    {
      $sql = "SELECT name FROM `".$config_databaseTablePrefix."categories` WHERE name='".database_safe(widget_posted($_POST["name"]))."'";

      if (database_querySelect($sql,$rows))
      {
        widget_errorSet("name","category name already exists");
      }
    }
    if (!widget_errorCount())
    {
      $sql = sprintf("INSERT INTO `".$config_databaseTablePrefix."categories` SET
                      name = '%s'
                      ",
                      database_safe(widget_posted($_POST["name"]))
                      );

      database_queryModify($sql,$insertId);

      header("Location: categories_configure.php?id=".$insertId);

      exit();
    }
  }

  require("admin_header.php");

  print "<h2>".translate("Category Mapping")."</h2>";

  print "<h3>".translate("New Category")."</h3>";

  widget_formBegin();

  widget_textBox("Name","name",TRUE,(isset($_POST["name"])?widget_posted($_POST["name"]):""),"",3);

  widget_formButtons(array("Add"=>TRUE));

  widget_formEnd();

  print "<h3>".translate("Existing Categories")."</h3>";

  $sql = "SELECT * FROM `".$config_databaseTablePrefix."categories` ORDER BY name";

  if (database_querySelect($sql,$rows))
  {
    print "<table>";

    foreach($rows as $category)
    {
      print "<tr>";

      print "<th class='pta_key'>".$category["name"]."</th>";

      print "<td>";

      admin_tool("Configure","categories_configure.php?id=".$category["id"],TRUE,FALSE);

      admin_tool("Delete","categories_delete.php?id=".$category["id"],TRUE,FALSE);

      print "</td>";

      print "</tr>";
    }

    print "</table>";
  }
  else
  {
    print "<p>".translate("There are no categories to display.")."</p>";
  }

  require("admin_footer.php");
?>
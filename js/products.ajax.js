'use strict';

var scroll = {
		cur_page: 1,
		search_url: location.href,
		no_more_data: false,
		on_ajax_start: function() {
				$('body').css('cursor', 'wait');
				//$(".main-wrapper").append($("#loading_image_template").html())
		},

		on_ajax_stop: function() {
			$('body').css('cursor', 'auto');
			//$(".main-wrapper #loading_image").remove()
		},

		search_url_changed_to: function(new_search_url,after_products_updated) {
			this.cur_page = 0;
			this.no_more_data = false;
			this.search_url = new_search_url;
			this.get_next_page(function() {
				$("ul.pt_sr").html("");
			},after_products_updated);
		},

		append_scroll_html: function(resp) {

				scroll.on_ajax_stop();
				//extracting only html we need
				var temp_div = $("#endless_scroll_service_div");
				temp_div.html(stripScripts(resp));

				var html = $("#endless_scroll_service_div ul.pt_sr").html()

				if($(html).text()=="") {
					console.log("no results page received");
					scroll.no_more_data = true;
					return;
				}

				$("ul.pt_sr").append(html)

				$(".under_filter_wrapper").html(temp_div.find(".under_filter_wrapper").html());
				$(".navigation").html(temp_div.find(".navigation").html());
				$(".sorting").html(temp_div.find(".sorting").html())
				console.log("sorting",temp_div.find(".sorting").html())

				$("img.pt_sr_image").lazyload({
				        event : "sporty",
				        data_attribute : "image",
				        placeholder: "/images/loading.gif"
				});
				
				adjust_product_colors()
		},

		getting_next_page: false,

		get_next_page: function(before_append_html,after_html_received) {
			if(scroll.no_more_data) {
				return;
			}
			if(scroll.getting_next_page) {
				console.log("getting new page skipped")
				return;
			}
			scroll.getting_next_page = true;

			scroll.cur_page++;
			console.log("getting page: "+scroll.cur_page)

			scroll.on_ajax_start();

			$.get(scroll.search_url, 
				{ 
					page:scroll.cur_page,
					for_endless_scroll:true,
					force_nocaching_timestamp:(new Date().getTime())
				}, function(resp) {
					before_append_html && before_append_html();
					scroll.append_scroll_html(resp)
					after_html_received && after_html_received($("#endless_scroll_service_div"))
					
					scroll.getting_next_page = false;
				})

		}


};




function stripScripts(s) {
  var div = document.createElement('div');
  div.innerHTML = s;
  var scripts = div.getElementsByTagName('script');
  var i = scripts.length;
  while (i--) {
    scripts[i].parentNode.removeChild(scripts[i]);
  }
  return div.innerHTML;
}


$(function() {

/*	$(window).scroll(function () { 
	   if ($(window).scrollTop() >= $(document).height() - $(window).height() - $(window).height()) {
	   	  scroll.get_next_page()
	   }
	});	*/


})

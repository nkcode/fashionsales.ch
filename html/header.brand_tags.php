<? 


//this parameter is set in .htaccess file
if(isset($_GET['catalog_by_brand'])) {

	$brand_name = $_GET['catalog_by_brand'];
	$brand_name = str_replace("-"," ",$brand_name);

	$rows = Product::selectRaw("MAX((price_old-price)/price_old)")
		->where("brand","=",$brand_name)->get();

	$max_discount = array_values($rows[0]["attributes"]);
	$max_discount = floatval($max_discount[0]);
	$percent = floor($max_discount*100);

	$brand_name = ucfirst($brand_name);

	$title = str_replace("[Brand]", $brand_name, $config_brandTitle);
	$title = str_replace("[MaxPercent]", $percent, $title);
	$title = str_replace("[Category]", isset($category_name) ? $category_name : 'Mode', $title);

	$description = str_replace("[Brand]",$brand_name,$config_brandDescription);
	$description = str_replace("[MaxPercent]",$percent,$description);
	$description = str_replace("[Category]", isset($category_name) ? $category_name : 'Mode', $description);

	$header['title'] = $title;
	$header['meta']['description'] = $description;

}

?>
<div class='pt_ba row'>

  <div class='small-12 columns'>

    <hr />
    <h1 class='shop_title'>
    	<? if($SHOP_TITLE_IMG): ?>
          <a class=image_url href=<?= $SHOP_TITLE_IMG_URL ?>>
    	    <span class=image><img src=<?= $SHOP_TITLE_IMG ?>></span>
          </a>
    	<? endif; ?>

    	<? if($SHOP_TITLE): ?>

    		<span class=title><?= $SHOP_TITLE ?></span>
    	<? endif; ?>
    </h1>

  </div>

</div>
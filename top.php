<?php
require("includes/common.php");

$header["title"] = "Top 100 search results.";

$header["meta"]["description"] = "Top 100 search results.";

$header["meta"]["keywords"] = "Top 100 search results.";
$page = (isset($_GET["page"])?intval($_GET["page"]):1);

require("html/header.php");

require("html/menu.php");

require("html/searchform.php");

require("html/banner.php");
$offset = ($page-1) * 100;
$sql = 'SELECT SQL_CALC_FOUND_ROWS id, COUNT(id) AS numMerchants,MIN(price) as minPrice, price_old, color, size, gender, shipping_time, name,normalised_name,image_url,description,rating, (MIN(price)*100/price_old) as percent
    FROM `pt_products`
    GROUP BY id
    ORDER BY percent ASC
    LIMIT '.$offset.', 100';

$numRows = database_querySelect($sql,$rows);
$searchresults["products"] = $rows;
$searchresults["numRows"] = $numRows;

$sqlResultCount = "SELECT FOUND_ROWS() as resultcount";
database_querySelect($sqlResultCount,$rowsResultCount);
$resultCount = $rowsResultCount[0]["resultcount"];
$navigation["resultCount"] = $resultCount;

foreach($searchresults["products"] as $k => $product)
{
    $searchresults["products"][$k]["productHREF"] = tapestry_productHREF($searchresults["products"][$k]);
}
/*if ($config_enableSearchFilters)
{
    $where = '1=1 ';
    require("html/searchfilters.php");
}*/
require("html/searchresults.php");
$config_resultsPerPage = 100;
require("html/navigation.php");
require("html/footer.php");
?>
DROP TABLE IF EXISTS `pt_product_sizes`;

CREATE TABLE `pt_product_sizes` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `size_extracted` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `pt_product_sizes`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `pt_product_sizes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
<?php
  set_time_limit(0);

  require("../includes/common.php");

  require("../includes/admin.php");

  $argc = $_SERVER["argc"];

  $argv = $_SERVER["argv"];

  if ($argc <> 3)
  {
    print "Usage: copyfilters.php <sourceFilename> <destinationFilename>\n"; exit;
  }

  $sourceFilename = $argv[1];

  $destinationFilename = $argv[2];

  $result = admin_copyFilters($sourceFilename,$destinationFilename);

  print $result."\n";

  exit();
?>
<?php
  require("includes/common.php");

  $now = time();

  $sql = "SELECT * FROM `".$config_databaseTablePrefix."vouchers` WHERE ( (valid_from < '".$now."' AND valid_to = '0') OR (valid_from <= '".$now."' AND valid_to > '".$now."') ) ORDER BY merchant";

  if (database_querySelect($sql,$vouchers))
  {
    foreach($vouchers as $k => $voucher)
    {
      if (file_exists("logos/".$voucher["merchant"]))
      {
        $vouchers[$k]["logo"] = $config_baseHREF."logos/".str_replace(" ","%20",$voucher["merchant"]);
      }

      $vouchers[$k]["href"] = strtolower($config_baseHREF."search.php?q=voucher:".urlencode($voucher["merchant"]).":".urlencode($voucher["code"]));

      switch($voucher["discount_type"])
      {
        case "#":

          $vouchers[$k]["text"] = translate("Save")." ".$config_currencyHTML.$voucher["discount_value"];

          break;

        case "%":

          $vouchers[$k]["text"] = translate("Save")." ".$voucher["discount_value"]."%";

          break;

        case "S":

          $vouchers[$k]["text"] = $voucher["discount_text"];

          break;
      }

      if ($voucher["min_price"] > 0)
      {
        $vouchers[$k]["text"] .= " ".translate("when you spend")." ".$config_currencyHTML.$voucher["min_price"];
      }

      if ($voucher["match_value"])
      {
        $vouchers[$k]["text"] .= " ".translate("on selected products");
      }

      $vouchers[$k]["text"] .= " ".translate("using voucher code")." <strong>".$voucher["code"]."</strong>";
    }
    $coupons["vouchers"] = $vouchers;
  }
  $banner["h2"] = "<strong>".translate("Voucher Codes")."</strong>";

  require("html/header.php");

  require("html/menu.php");

  require("html/searchform.php");

  require("html/banner.php");

  require("html/coupons.php");

  require("html/footer.php");
?>
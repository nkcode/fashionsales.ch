<?php
  function tapestry_search($text)
  {
    $text = str_replace(" ","",strtolower($text));

    return $text;
  }

  function tapestry_hyphenate($text)
  {
    $text = str_replace(" ","-",$text);

    return $text;
  }

  function tapestry_normalise($text,$allow = "")
  {
    global $config_charset;

    $text = str_replace("-"," ",$text);

    if ($config_charset)
    {
      $allow = chr(0x80).'-'.chr(0xFF).$allow;
    }

    $text = preg_replace('/[^A-Za-z0-9'.$allow.' ]/','',$text);

    $text = preg_replace('/[ ]{2,}/',' ',$text);

    return trim($text);
  }

  function tapestry_decimalise($price)
  {
    if (strpos($price,","))
    {
      if (strpos($price,",") > strpos($price,"."))
      {
        $price = str_replace(".","",$price);

        $price = str_replace(",",".",$price);
      }
      elseif (strpos($price,".") > strpos($price,","))
      {
        $price = str_replace(",","",$price);
      }
      else
      {
        $price = str_replace(",",".",$price);
      }
    }

    $price = preg_replace('/[^0-9\.]/','',$price);

    $price = sprintf("%.2f",$price);

    return $price;
  }

  function tapestry_productHREF($product)
  {
    global $config_baseHREF;

    global $config_useRewrite;

    if ($config_useRewrite)
    {
      return $config_baseHREF."p/".urlencode(tapestry_hyphenate($product["normalised_name"])).".html";
    }
    else
    {
      return $config_baseHREF."products.php?q=".urlencode($product["normalised_name"]);
    }
  }

  function tapestry_reviewHREF($product)
  {
    global $config_baseHREF;

    global $config_useRewrite;

    if ($config_useRewrite)
    {
      return $config_baseHREF."review/".urlencode(tapestry_hyphenate($product["normalised_name"])).".html";
    }
    else
    {
      return $config_baseHREF."reviews.php?q=".urlencode($product["normalised_name"]);
    }
  }

  function tapestry_buyURL($product)
  {
    global $config_baseHREF;

    global $config_useTracking;

    if ($config_useTracking)
    {
      return $config_baseHREF."jump.php?id=".$product["id"];
    }
    else
    {
      return $product["buy_url"];
    }
  }

  function tapestry_stars($stars,$imagePostfix)
  {
    global $config_baseHREF;

    $html = "<img src='".$config_baseHREF."images/".$stars.$imagePostfix.".gif' alt='".$stars." ".translate("Star Rating")."' />";

    return $html;
  }

  function tapestry_priceDate($merchant)
  {
    global $config_databaseTablePrefix;

    $sql = "SELECT imported FROM `".$config_databaseTablePrefix."feeds` WHERE merchant='".$merchant."'";

    database_querySelect($sql,$rows);

    return date("d/m/y",$rows[0]["imported"]);
  }

  function tapestry_applyVoucherCodes($products)
  {
    global $tapestry_voucherCodes;

    global $config_databaseTablePrefix;

    $ins = array();

    foreach($products as $product)
    {
      if (!isset($tapestry_voucherCodes[$product["merchant"]]))
      {
        $ins[$product["merchant"]] = "'".database_safe($product["merchant"])."'";
      }
      if (count($ins))
      {
        $in = implode(",",$ins);

        $sql = "SELECT * FROM `".$config_databaseTablePrefix."vouchers` WHERE merchant IN (".$in.")";

        database_querySelect($sql,$vouchers);

        foreach($vouchers as $voucher)
        {
          $now = time();

          if ($voucher["valid_from"] > $now) continue;

          if ($voucher["valid_to"]) if ($voucher["valid_to"] < $now) continue;

          $tapestry_voucherCodes[$voucher["merchant"]][] = $voucher;
        }
      }
    }

    foreach($products as $k => $product)
    {
      if (isset($tapestry_voucherCodes[$product["merchant"]]))
      {
        $product["discount_price"] = $product["price"];

        foreach($tapestry_voucherCodes[$product["merchant"]] as $voucher)
        {
          $isValid = TRUE;

          if ($isValid)
          {
            if (
               ($voucher["min_price"])
               &&
               ($voucher["min_price"] > $product["price"])
               )
            {
              $isValid = FALSE;
            }
          }

          if ($isValid)
          {
            if ($voucher["match_value"])
            {
              $matched = FALSE;

              $match_values = explode(",",$voucher["match_value"]);

              foreach($match_values as $match_value)
              {
                switch($voucher["match_type"])
                {
                  case "exact":

                    if ($product[$voucher["match_field"]] == $match_value)
                    {
                      $matched = TRUE;
                    }

                    break;

                  case "keyword":

                    if (stripos($product[$voucher["match_field"]],$match_value) !== FALSE)
                    {
                      $matched = TRUE;
                    }

                    break;
                }
              }
              $isValid = $matched;
            }
          }

          if ($isValid)
          {
            if ($voucher["discount_type"]=="#")
            {
              $discountAmount = $voucher["discount_value"];

              $discountPrice = tapestry_decimalise($product["price"] - $discountAmount);
            }
            elseif ($voucher["discount_type"]=="%")
            {
              $discountAmount = $product["price"]*($voucher["discount_value"]/100);

              $discountPrice = tapestry_decimalise($product["price"] - $discountAmount);
            }
            elseif ($voucher["discount_type"]=="S")
            {
              $discountPrice = $product["price"];
            }

            if (
               ($discountPrice <= $product["discount_price"])
               )
            {
              $product["discount_price"] = $discountPrice;

              $product["voucher_code"] = $voucher["code"];

              if ($voucher["discount_text"])
              {
                $product["voucher_code"] .= " (".$voucher["discount_text"].")";
              }
            }
          }
        }

        if (isset($product["voucher_code"]))
        {
          $products[$k]["price"] = $product["discount_price"];

          $products[$k]["voucher_code"] = $product["voucher_code"];
        }
      }
    }
    return $products;
  }

  function tapestry_substr($text,$length,$append="")
  {
    if (strlen($text) > $length)
    {
      $breakOffset = strpos($text," ",$length);

      if ($breakOffset !== FALSE)
      {
        $text = substr($text,0,$breakOffset).$append;
      }
    }
    return $text;
  }

  function tapestry_mb_substr($text,$pos,$len)
  {
    global $config_charset;

    return (function_exists("mb_substr")?mb_substr($text,$pos,$len,$config_charset):substr($text,$pos,$len));
  }

  function tapestry_mb_strtoupper($text)
  {
    global $config_charset;

    return (function_exists("mb_strtoupper")?mb_strtoupper($text,$config_charset):strtoupper($text));
  }

  function tapestry_price($price)
  {
    global $config_currencyHTML;

    return $config_currencyHTML.$price;
  }
?>
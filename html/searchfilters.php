<script src="/bower_components/foundation-multiselect/zmultiselect/zurb5-multiselect.js?2"></script>
<link rel='stylesheet' href='/bower_components/foundation-multiselect/zmultiselect/zurb5-multiselect.css' />

<script type='text/JavaScript'>

  function pt_sf_small_onClick()
  {
    document.getElementById("pt_sf_small").innerHTML = "";
    document.getElementById("pt_sf_medium").className = "";
  }

var PRODUCTS_MAX_PRICE = <?= $products_max_price ?>;
var PRODUCTS_MIN_PRICE = <?= $products_min_price ?>;
function ajax_get_products(filter_control) {
  window.location.hash = $("form[name=filters]").serialize();
  scroll.search_url_changed_to("/search.php?"+$("form[name=filters]").serialize(),function(html_container) {
    console.log("got html",html_container.html().length)
    console.log("filter changed:",filter_control.attr("id"))

    PRODUCTS_MAX_PRICE = parseInt(html_container.find("#products_max_price").html()) || 0;
    PRODUCTS_MIN_PRICE = parseInt(html_container.find("#products_min_price").html()) || 0;
    console.log("products_max_price",PRODUCTS_MAX_PRICE)



    if(filter_control.attr("id")!="price_slider") {
      $("#price_slider").slider({min:PRODUCTS_MIN_PRICE, max:PRODUCTS_MAX_PRICE});
      $("#price_slider").slider('option', 'slide')(null,{values:[PRODUCTS_MIN_PRICE,PRODUCTS_MAX_PRICE]})
    }

    html_container.find(".multiselect").each(function() {
      if($(this).attr("id")!=filter_control.attr("id")) {
        var filter_id = $(this).attr("id");
        console.log('updating filter',filter_id)
        target_zselect = $(".multiselect[id="+filter_id+"]").next();
        target_zselect.find("li.filtered-out").removeClass("filtered-out")

        var got_values = [];
        var got_counts = [];
        $(this).find("option").each(function() {
          var val = $(this).attr("value");
          got_values.push(val)
          got_counts.push($(this).text())
        })
        console.log(got_values,got_counts)

        $(target_zselect).find("input[type=checkbox]").each(function() {
          var val = $(this).attr('value');
          var i = got_values.indexOf(val);
          if(i==-1) {
            $(this).parents("li").addClass("filtered-out");
          } else {
            $(this).parents("li").find("span").html(got_counts[i])
          }
        })

      }

    })


  })
}

function apply_zmultiselect(multiselect) {
    multiselect.zmultiselect({
      selectedText: ['', 'aus'],
      live: "[name="+multiselect.attr("data-target-input-name")+"]",
      filter: true,
      selectAll: false,
      placeholder: multiselect.attr("data-placeholder")
    })
  //update 1 aus 10
  $(".zselect input:checkbox:eq(0)").trigger("change");

}

$(function() {



  if(location.hash) {
    console.log("got hash, converting hash to query")
    var hash = location.hash;
    location.hash = "";
    location.search = hash.replace(/^#/,"?");
    return;
  }


  $(".multiselect.got_options").each(function() {
    apply_zmultiselect($(this));
  })

  $(document).on('click', '.zselect li', function(e){ 
    ajax_get_products($(this).parents(".zselect").prev());
  })


  var minprice = $("input[name=minPrice]").val() || PRODUCTS_MIN_PRICE;
  var maxprice = $("input[name=maxPrice]").val() || PRODUCTS_MAX_PRICE;
  console.log("minprice",minprice)
  console.log("products_max_price",PRODUCTS_MAX_PRICE)
  console.log("maxprice",maxprice)

  $("#price_slider").slider({
      range: true,
      min: PRODUCTS_MIN_PRICE,
      max: PRODUCTS_MAX_PRICE,
      values: [minprice,maxprice],
      stop: function() {
        ajax_get_products($("#price_slider"));
      },
      slide: function( event, ui ) {
        $(".price_range #minPrice").html(ui.values[0])
        $(".price_range #maxPrice").html(ui.values[1])
        var min = ui.values[0];
        var max = ui.values[1];
        if(min==PRODUCTS_MIN_PRICE) {
          min = "";
        }
        if(max==PRODUCTS_MAX_PRICE) {
          max = "";
        }
        $(".price_range input[name=minPrice]").val(min)
        $(".price_range input[name=maxPrice]").val(max)
      }
    });


})

</script>

<div id='pt_sf_small' class='show-for-small-only'>

  <div class='row'>

    <div class='small-12 columns pt_sf_small'>

    <button onclick='JavaScript:pt_sf_small_onClick();' class='button tiny secondary'><?= translate("Filter These Results") ?> &darr;</button>

    </div>

  </div>

</div>

<div id='pt_sf_medium' class='show-for-medium-up'>
  <div class='row pt_sf'>
  
<div class="search-filters-wrapper">  
  
<div class="search-filters">


<script>
$(function() {
  $(".navigate-on-click").change(function(e) {
    window.location = $(this).val();
  })
})
</script>



<form method='GET' name='filters'>

<input type='hidden' name='q' value='<?= htmlspecialchars($q,ENT_QUOTES,$config_charset) ?>' />

<div class="price_range_wrapper small-12 medium-12 columns">
  <span style='display:none' id=products_max_price><?= $products_max_price ?></span>
  <span style='display:none' id=products_min_price><?= $products_min_price ?></span>
  
  <div class="filters-logo">
      <a class="logo" href="/"></a>
  </div>

  <div class="small-12 medium-12 columns price_range" style='margin-bottom:10px;'>
      <div class=price_display>
        CHF <span id=minPrice><?= Input::get("minPrice")=="" ?  $products_min_price : Input::get("minPrice") ?></span>
      </div>
      <input type=hidden name=minPrice value="<?= Input::get("minPrice") ?>">
      <div class='price_slider_wrapper'>
          <div id='price_slider' style=''></div>
      </div>
      <div class=price_display>
        CHF <span id=maxPrice><?= Input::get("maxPrice")=="" ? $products_max_price : Input::get("maxPrice") ?></span>
      </div>
      <input type=hidden name=maxPrice value="<?= Input::get("maxPrice") ?>">
  </div>

</div>

<?


      if ($parts[0] != "merchant") {
        $hash = Product::count_by_attribute("merchant",$SHOP_WHERE, $where_by_attr);
        multiselect_input_tag("merchantFilter",$hash,"merchant",$merchant_list);
      }

      if ($parts[0] != "category") {
        $hash = Product::count_by_attribute("category",$SHOP_WHERE, $where_by_attr);
        multiselect_input_tag("categoryFilter",$hash,"category",$categories_list);
      }

      if ($parts[0] != "brand" ) {
        $hash = Product::count_by_attribute("brand",$SHOP_WHERE, $where_by_attr);
        multiselect_input_tag("brandFilter", $hash, "brand", $brands_list);
      }


      $rows = Product::count_by_attribute("color",$SHOP_WHERE, $where_by_attr);
      $base_rows = Product::count_by_attribute("color",$SHOP_WHERE, array());
      $rows = Product::color_fix_for_enums($rows);
      $base_rows = Product::color_fix_for_enums($base_rows);
      multiselect_input_tag("colorFilter", $rows, "color", $colors_list,$base_rows);

      $hash = Product::count_by_sizes($SHOP_WHERE, $where_by_attr);
      multiselect_input_tag("sizeFilter", $hash, "size", $sizes_list);

      $rows = Product::count_by_attribute("gender",$SHOP_WHERE, $where_by_attr);
      $base_rows = Product::count_by_attribute("gender",$SHOP_WHERE, array());
      multiselect_input_tag("genderFilter", $rows, "gender", $genders_list, $base_rows);


?>

<div class='small-12 medium-2 columns'>

<button type='submit' class='button tiny pt_sf_submit'><?= translate("Filter These Results") ?></button>

</div>

  </form>
  </div>
  </div>
  
  <div style='clear:both'></div>
  
  </div>
</div>

</div>

</div>

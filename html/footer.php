    </div>
	
	<div class="footer-wrapper">
	
	<div class="footer row">
	
	  <div class="copyright small-12 medium-6 columns">Copyright &#169; Fashionsales</div>
	  
	  <div class="small-12 medium-6 columns">
	  
	  <ul class="footer-menu">
	     <li><a href="/kontakt.php">Impressum</a></li>
		 <li><a href="/kontakt.php">Kontakt</a></li>
		 <li><a href="#">Über Uns</a></li>
	  </ul>
	  
	  </div>
	
	</div>
	
	</div>
	
	<span class="scroll-top"></span>
	
	<?php
      if (file_exists("html/user_footer_before.php")) require("html/user_footer_before.php");
    ?>
		<script src='/html/vendor/jquery.custom.js'></script>
    <script src='<?php print $config_baseHREF; ?>html/vendor/foundation.min.js'></script>
    <script>
      $(document).foundation();
    </script>
    <? require "html/_footer.lazyload.php" ?>
    <? require "html/_footer.search.autocomplete.php" ?>
		<? require "html/_footer.colors_from_palette.php" ?>
		<? require "html/_footer.endless_scroll.php" ?>

	<script>
	  $( ".menu-toggler" ).click(function() {
          $( ".menu-wrapper" ).toggle();
      });
    </script>
	
	<script>
	    
      $(document).ready(function(){
		
			$('.scroll-top').click(function () {
			
			  $('html, body').animate({scrollTop: 0}, 600);
			
			});
		
		$(window).scroll(function (event) {
		  var scroll = $(window).scrollTop() == 0;
		
		  $('.scroll-top').fadeIn();
		  $('.search-filters-wrapper').addClass('sf-fixed');
		  
		  if (scroll) {
		    
			$('.scroll-top').fadeOut();
			$('.search-filters-wrapper').removeClass('sf-fixed');
			
		  }
		  
		  if ($(window).width() < 640 ) {
		  
		    $('.search-filters-wrapper').removeClass('sf-fixed');
		  
		  }
		
		});
		
		$('.alle-categories').click(function () {
		
		  $('.hidden-categories').slideDown();
		
		});


      });
	
    </script>
	

  </body>

</html>
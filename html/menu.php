<div class="top-bar-wrapper row">
<nav class="top-bar" data-topbar role="navigation">

<section class="top-bar-section">

    <ul class="title-area">

    <li class="name">
      <a class="logo" href="/"></a>
    </li>

    </ul>
	
	<span class="menu-toggler" value='hide/show'>&#9776;</span>
    
	<div class="menu-wrapper">
    <ul class="main-menu right">

	  <li id="main-marken"><a href="<?php print $config_baseHREF.($config_useRewrite?"b/":"brands.php"); ?>">Unsere Marken</a>
	  
	    <ul class="submenu-brands">
			    <li><a href="/b/adidas-performance/"><img class="submenu_brands_image" data-image="/logos/submenu/adidas-performance.jpg" alt=""/></a></li>
				<li><a href="/b/alprausch/"><img class="submenu_brands_image" data-image="/logos/submenu/alprausch.jpg" alt=""/></a></li>
				<li><a href="/b/asics/"><img class="submenu_brands_image" data-image="/../logos/submenu/asics.jpg" alt=""/></a></li>
				<li><a href="/b/bally/"><img class="submenu_brands_image" data-image="/../logos/submenu/bally.jpg" alt=""/></a></li>
				<li><a href="/b/bench/"><img class="submenu_brands_image" data-image="/../logos/submenu/bench.jpg" alt=""/></a></li>
				<li><a href="/b/billabong/"><img class="submenu_brands_image" data-image="/../logos/submenu/billabong.jpg" alt=""/></a></li>
				<li><a href="/b/buffalo/"><img class="submenu_brands_image" data-image="/../logos/submenu/buffalo.jpg" alt=""/></a></li>
				<li><a href="/b/burton/"><img class="submenu_brands_image" data-image="/../logos/submenu/burton.jpg" alt=""/></a></li>
				<li><a href="/b/camper/"><img class="submenu_brands_image" data-image="/../logos/submenu/camper.jpg" alt=""/></a></li>
				<li><a href="/b/carhartt/"><img class="submenu_brands_image" data-image="/../logos/submenu/carhartt.jpg" alt=""/></a></li>
				<li><a href="/b/columbia/"><img class="submenu_brands_image" data-image="/../logos/submenu/columbia.jpg" alt=""/></a></li>
				<li><a href="/b/converse/"><img class="submenu_brands_image" data-image="/../logos/submenu/converse.jpg" alt=""/></a></li>
				<li><a href="/b/bcbgeneration/"><img class="submenu_brands_image" data-image="/../logos/submenu/bcbgeneration.jpg" alt=""/></a></li>
				<li><a href="/b/desigual/"><img class="submenu_brands_image" data-image="/../logos/submenu/desigual.jpg" alt=""/></a></li>
				<li><a href="/b/diesel/"><img class="submenu_brands_image" data-image="/../logos/submenu/diesel.jpg" alt=""/></a></li>
				<li><a href="/b/dkny/"><img class="submenu_brands_image" data-image="/../logos/submenu/dkny.jpg" alt=""/></a></li>
				<li><a href="/b/esprit/"><img class="submenu_brands_image" data-image="/../logos/submenu/esprit.jpg" alt=""/></a></li>
				<li><a href="/b/gant/"><img class="submenu_brands_image" data-image="/../logos/submenu/gant.jpg" alt=""/></a></li>
				<li><a href="/b/jack-wolfskin/"><img class="submenu_brands_image" data-image="/../logos/submenu/jack-wolfskin.jpg" alt=""/></a></li>
				<li><a href="/b/nike-performance/"><img class="submenu_brands_image" data-image="/../logos/submenu/nike-performance.jpg" alt=""/></a></li>
                
				<a class="all-link" href="/b/">Alle Marken</a>
			 
			 </ul>
          <script type="text/javascript">
              $(function() {
                  $("img.submenu_brands_image").lazyload({
                      event : "sporty-brands",
                      data_attribute : "image",
                      placeholder: "/images/loading.gif"
                  });
              });

              $('#main-marken').bind("mouseover", function() {
                  var timeout = setTimeout(function() { $("img.submenu_brands_image").trigger("sporty-brands") }, 10);
              });
          </script>
	  
	  </li>
	  <li id="main-kategorien"><a href="<?php print $config_baseHREF.($config_useRewrite?"k/":"categories.php"); ?>">Alle Kategorien</a>
	  
	    <ul class="submenu-brands submenu-categories">
			 
			    <li><a href="/k/abendkleider/"><span class="item-title">Abendkleider<span class="count"> ( 794 )</span></span></a></li>
				<li><a href="/k/kleider/"><span class="item-title">Kleider<span class="count"> ( 486 )</span></span></a></li>
				<li><a href="/k/gummistiefel/"><span class="item-title">Gummistiefel<span class="count"> ( 89 )</span></span></a></li>
				<li><a href="/k/handtaschen/"><span class="item-title">Handtaschen<span class="count"> ( 156 )</span></span></a></li>
                <li><a href="/k/blazer/"><span class="item-title">Blazer<span class="count"> ( 257 )</span></span></a></li>
				<li><a href="/k/sommerkleider/"><span class="item-title">Sommerkleider<span class="count"> ( 1208 )</span></span></a></li>
				<li><a href="/k/lederjacken/"><span class="item-title">Lederjacken<span class="count"> ( 328 )</span></span></a></li>
				<li><a href="/k/stiefel/"><span class="item-title">Stiefel<span class="count"> ( 106 )</span></span></a></li>
				<li><a href="/k/stiefeletten/"><span class="item-title">Stiefeletten<span class="count"> ( 162 )</span></span></a></li>
				<li><a href="/k/winterjacken/"><span class="item-title">Winterjacken<span class="count"> ( 270 )</span></span></a></li>
				<li><a href="/k/pullover/"><span class="item-title">Pullover<span class="count"> ( 472 )</span></span></a></li>
				<li><a href="/k/pumps/"><span class="item-title">Pumps<span class="count"> ( 310 )</span></span></a></li>
				<li><a href="/k/umhängetaschen/"><span class="item-title">Umhängetaschen<span class="count"> ( 201 )</span></span></a></li>
				<li><a href="/k/freizeit/"><span class="item-title">Freizeit<span class="count"> ( 1205 )</span></span></a></li>
				<li><a href="/k/t-shirts/"><span class="item-title">T Shirts<span class="count"> ( 2840 )</span></span></a></li>
				<li><a href="/k/daunenjacken/"><span class="item-title">Daunenjacken<span class="count"> ( 85 )</span></span></a></li>
				<li><a href="/k/strickjacken/"><span class="item-title">Strickjacken<span class="count"> ( 634 )</span></span></a></li>
				<li><a href="/k/tops/"><span class="item-title">Tops<span class="count"> ( 1466 )</span></span></a></li>
				<li><a href="/k/jeans/"><span class="item-title">Jeans<span class="count"> ( 412 )</span></span></a></li>
				<li><a href="/k/leggings/"><span class="item-title">Leggings<span class="count"> ( 217 )</span></span></a></li>
				
				<a class="all-link" href="/k/">Alle Kategorien</a>
			 
			 </ul>
	  
	  </li>
	  <li id="main-damen"><a href="/search.php?q=gender%3Adamen%3A">Damenmode</a></li>
	  <li id="main-herren"><a href="/search.php?q=gender%3Aherren%3A">Herrenmode</a></li>
	  <li id="main-madchen"><a href="/search.php?q=gender%3Am%C3%A4dchen%3A">Mädels</a></li>
	  <li id="main-jungen"><a href="/search.php?q=gender%3Ajungen%3A">Jungs</a></li>
	  
	</ul>
	</div>
  
</section>

</nav>
</div>
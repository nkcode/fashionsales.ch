<?php
  set_time_limit(0);

  require("../includes/common.php");

  if (isset($_SERVER["REQUEST_METHOD"]))
  {
    $password = (isset($_GET["password"])?$_GET["password"]:"");

    if ($password != $config_adminPassword)
    {
      header('HTTP/1.0 401 Unauthorized');

      print "<h1>401 Unauthorized</h1>";

      exit();
    }

    header("Content-Type: text/plain");
  }

  require("../includes/admin.php");

  require("../includes/automation.php");

  require("../includes/filter.php");

  require("../includes/MagicParser.php");

  function callback($progress)
  {
    global $feed;

    print chr(13)."importing ".$feed["filename"]."...[".$progress."/".$feed["products"]."]";
  }

  function fetch()
  {
    global $job;

    print chr(13)."fetching ".$job["filename"];

    $status = automation_run($job["id"]);

    print chr(13)."fetching ".$job["filename"]."...[".$status."]            \n";
  }

  function import()
  {
    global $feed;

    global $limit;

    print chr(13)."importing ".$feed["filename"]."...[0/".$feed["products"]."]";

    admin_import($feed["filename"],$limit,"callback");

    print chr(13)."importing ".$feed["filename"]."...[done]            \n";
  }

  $sql = "SELECT * FROM `".$config_databaseTablePrefix."jobs` ORDER BY filename";

  if (database_querySelect($sql,$jobs))
  {
    foreach($jobs as $job)
    {
      fetch();
    }
  }

  $admin_importAll = TRUE;

  $sql = "DROP TABLE IF EXISTS `".$config_databaseTablePrefix."products_import`";

  database_queryModify($sql,$result);

  $sql = "CREATE TABLE `".$config_databaseTablePrefix."products_import` LIKE `".$config_databaseTablePrefix."products`";

  database_queryModify($sql,$result);

  $sql = "ALTER TABLE `".$config_databaseTablePrefix."products_import` DISABLE KEYS";

  database_queryModify($sql,$result);

  $sql = "SELECT * FROM `".$config_databaseTablePrefix."feeds`";

  if (database_querySelect($sql,$rows))
  {
    foreach($rows as $feed)
    {
      if (file_exists($config_feedDirectory.$feed["filename"]))
      {
        import();
      }
    }
  }
  $sql = "ALTER TABLE `".$config_databaseTablePrefix."products_import` ENABLE KEYS";

  database_queryModify($sql,$result);

  $sql = "DROP TABLE `".$config_databaseTablePrefix."products`";

  database_queryModify($sql,$result);

  $sql = "RENAME TABLE `".$config_databaseTablePrefix."products_import` TO `".$config_databaseTablePrefix."products`";

  database_queryModify($sql,$result);

  print chr(13)."backfilling reviews...                ";

  admin_importReviews();

  print chr(13)."backfilling reviews...[done]\n";

  exit();
?>
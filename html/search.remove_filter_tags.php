<style>
  .filter_tag {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 5px;
    border-radius: 4px;
    cursor: pointer;
    color: #000;
  }
  .filter_tag:hover {
    background: #eee;
  }
</style>

<script>
  function removeParamVal(param_name,val) {
    var params = JSON.parse('{"' + decodeURI(location.search).replace(/^\?/,'').replace(/&/g, "\",\"").replace(/=/g,"\":\"") + '"}')
    console.log(params)
    for(var key in params) { 
      params[key] = decodeURIComponent(params[key]).replace(/\+/g,' ')
    }
    var vals = decodeURIComponent(params[param_name]).split(",")
    for(var i=0;i<vals.length;i++) {
      vals[i] = decodeURIComponent(vals[i])
    }
    console.log(vals)
    vals = $.grep(vals,function(got_val) {
      return got_val != val;
    })

    params[param_name] = vals.join(",")
    console.log("params object after replace",params)
    console.log(params)
    return $.param(params);
  }

  $(function() {
    $(".filter_tag[data-remove-val]").click(function() {
      var location = "?"+removeParamVal($(this).attr("data-param-name"),$(this).attr("data-remove-val"))
      console.log("location",location)
      window.location = location;
    })
  })

</script>

<div class='filter_tags' style='max-width: 1200px;margin: 0 auto;padding:14px;'>


    <?foreach($merchants_list as $name) { ?>
    <div class='filter_tag merchant' data-param-name=brandFilter data-remove-val='<?= $name ?>'>
      brand: <?= $name ?> &nbsp; x
    </div>
    <? } ?>

    <?if(!Input::get("catalog_by_category")): ?>
      <?foreach($categories_list as $category) { ?>
      <div class='filter_tag category' data-param-name=categoryFilter data-remove-val='<?= $category ?>'>
        category: <?= $category ?> &nbsp; x
      </div>
      <? } ?>
    <? endif; ?>

    <?foreach($brands_list as $name) { ?>
    <div class='filter_tag brand' data-param-name=brandFilter data-remove-val='<?= $name ?>'>
      brand: <?= $name ?> &nbsp; x
    </div>
    <? } ?>

    <?foreach($colors_list as $name) { ?>
    <div class='filter_tag color' data-param-name=colorFilter data-remove-val='<?= $name ?>'>
      color: <?= $name ?> &nbsp; x
    </div>
    <? } ?>


    <?foreach($genders_list as $name) { ?>
    <div class='filter_tag' data-param-name=genderFilter data-remove-val='<?= $name ?>'>
      gender: <?= $name ?> &nbsp; x
    </div>
    <? } ?>

    <?foreach($sizes_list as $name) { ?>
    <div class='filter_tag' data-param-name=sizesFilter data-remove-val='<?= $name ?>'>
      size: <?= $name ?> &nbsp; x
    </div>
    <? } ?>

  </div>



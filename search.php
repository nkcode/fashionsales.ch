<?php

  require("includes/common.php");

  if(Input::get("catalog_by_brand")) {
    $temp = $_GET['catalog_by_brand'];
    $exp = explode('/', $temp);
    if(is_array($exp) && count($exp)>1){
        $_GET['catalog_by_brand'] = $exp[0];
        $_GET['catalog_by_category'] = $_GET['categoryFilter'] = $exp[1];
        $_GET['q'] = str_replace($temp, $exp[0], $_GET['q']);
    }
  }

  require("includes/stopwords.php");

  foreach(array('catalog_by_brand','categoryFilter','catalog_by_category') as $param_name) {
    if(isset($_GET[$param_name])) {
      $_GET[$param_name] = str_replace("-"," ",$_GET[$param_name]);
    }
  }

  if(strpos($_SERVER['REQUEST_URI'],"search.php") !== FALSE) {
    $ROBOTS_TAG = "noindex, nofollow";
  }

  $q = (isset($_GET["q"])?tapestry_normalise($_GET["q"],":\."):"");

  $page = intval(Input::get("page","1"));
  if($page>1) {
    $ROBOTS_TAG = "noindex, follow";
  }

  $sort = Input::get("sort","percentAsc");


  $rewrite = isset($_GET["rewrite"]);




  $FILTER_PARAMS_LIST = array("merchantFilter","categoryFilter","brandFilter","colorFilter","genderFilter","sizeFilter","minPrice","maxPrice");
  foreach($FILTER_PARAMS_LIST as $param) {
    $$param = Input::get($param);
  }

  $where_by_attr = array();

  if ($minPrice || $maxPrice) {
    if ($minPrice && $maxPrice) {
      $where_by_attr["price"] = "(price > {$minPrice}) AND (price < {$maxPrice})";
    } elseif ($minPrice) {
      $where_by_attr["price"] = "price > {$minPrice}";
    } elseif ($maxPrice) {
      $where_by_attr["price"] = "price < {$maxPrice}";
    }
  }

  if ($merchantFilter) {
    $merchants_list = filter_string_to_array($merchantFilter);
    $values = "'".implode("','",$merchants_list)."'"; 
    $where_by_attr["merchant"] = "merchant IN (".$values.") ";
  }

  list($name, $value) = explode(':', $_GET['q']);

  if($name=='category' && $categoryFilter=='')
    $categoryFilter = str_replace("-"," ",$value);

  if ($categoryFilter) {
    $categoryFilter = str_replace("-"," ",$categoryFilter);
    $categories_list = filter_string_to_array($categoryFilter);
    $values ="'".implode("','",$categories_list)."'";
    $where_by_attr['category'] = "category IN (".$values.") ";
  }


  if ($brandFilter) {
    $brands_list = filter_string_to_array($brandFilter);
    $values = "'".implode("','",$brands_list)."'";
    $where_by_attr['brand'] = "brand IN (".$values.")";
  }

  //i need this field to add: Price_old, ShippingTime, color, size, gender, ImgUrl2, ImgUrl3, ImgUrl4if ($merchantFilter)
  if($colorFilter) {
    $colors_list = filter_string_to_array($colorFilter);
    $colors_where = array();
    foreach($colors_list as $color) {
      array_push($colors_where, " (color like \"%".database_safe($color)."%\") ");
    }
    $values = "'".implode("','",$colors_list)."'";
    $where_by_attr["color"] = implode(" OR ",$colors_where);
  }

  if($sizeFilter) {
    $sizes_list =  filter_string_to_array($sizeFilter);
    $sizes_likes = array();
    foreach($sizes_list as $size) {
      $size = database_safe($size);
      $sizes_likes[] = "(size like '%".$size."|%' OR size like '%|".$size."%' OR size='".$size."')";
    }
    $where_by_attr["size"] = join(" OR ",$sizes_likes);

  }

  if($genderFilter) {
    $genders_list = filter_string_to_array($genderFilter);
    $values = "'".implode("','",$genders_list)."'";
    $where_by_attr["gender"] = "gender IN (".$values.") ";
  }

  if(count($where_by_attr)) {
    $FILTERS_WHERE = " AND (".join(") AND (",array_values($where_by_attr)).")";
  }

  switch($sort) {
    case "priceAsc":
      $SQL_ORDER = "price ASC";
      break;
    case "percentAsc":
      $SQL_ORDER = "percent ASC";
      break;
    default:
      $SQL_ORDER = "";
      break;
  }

  if ($q) {

    $parts = explode(":",$q);

    $SQL_GROUP = "search_name";

    switch($parts[0]) {
      case "color":
      case "size":
      case "gender":
      case "merchant":
        // pass through to category
      case "category":
        // pass through to brand
      case "brand":


        $fields = array("merchant","category","brand","color","size","gender");
        $first = TRUE;
        $SHOP_WHERE = "";
        $j = count($parts);

        for($i=0;$i<$j;$i++) {
          if (!$parts[$i]) continue;

          if (in_array($parts[$i],$fields)) {
            $field = $parts[$i];
            continue;
          }

          if (!$first) {
            $SHOP_WHERE .= " AND ";
          }


          if($field=='category')
            $SHOP_WHERE .= " ".$field." like '%".database_safe($parts[$i])."' ";
          else
            $SHOP_WHERE .= " ".$field." = '".database_safe($parts[$i])."' ";

          $first = FALSE;
        }


        $SQL_FIELDS = "SQL_CALC_FOUND_ROWS id,COUNT(id) AS numMerchants,MIN(price) as minPrice, ROUND( (MIN(price)*100/price_old) ) as percent, price_old, color, size, gender, shipping_time";

        break;

      default:


        $words = explode(" ",$parts[0]);

        if ($config_useFullText) {
          foreach($words as $word) {
            if (strlen($word) <= 3 || in_array(strtolower($word),$stopwords)) {
              $config_useFullText = FALSE;

              break;
            }
          }
        }


        if ($config_useFullText) {
          $words = explode(" ",$parts[0]);

          $newWords = array();

          foreach($words as $word) {
            if (substr($word,-1)=="s") {
              $newWords[] = substr($word,0,-1);
            }
          }

          $allWords = array_merge($words,$newWords);

          $parts[0] = implode(" ",$allWords);

          if ($config_searchDescription) {
            $matchFields = "name,description";
          } else {
            $matchFields = "name";
          }

          $SHOP_WHERE = "MATCH ".$matchFields." AGAINST ('".database_safe($parts[0])."')";

          $SQL_FIELDS = "SQL_CALC_FOUND_ROWS id,COUNT(id) AS numMerchants,MIN(price) as minPrice, ROUND( (MIN(price)*100/price_old) ) as percent, price_old, color, size, gender, shipping_time, MATCH ".$matchFields." AGAINST ('".database_safe($parts[0])."') AS relevance";

          $orderBySelection = $orderByFullText;
        } else {
          $wheres = array();

          foreach($words as $word) {
            $SHOP_WHERE  = "(";

            $SHOP_WHERE .= "search_name LIKE '%".database_safe($word)."%'";

            if ($config_searchDescription) {
              $SHOP_WHERE .= " OR description LIKE '%".database_safe($word)."%'";
            }

            if (substr($word,-1)=="s") {
              $SHOP_WHERE .= " OR search_name LIKE '%".database_safe(substr($word,0,-1))."%'";

              if ($config_searchDescription) {
                $SHOP_WHERE .= " OR description LIKE '%".database_safe(substr($word,0,-1))."%'";
              }
            }

            $SHOP_WHERE .= ")";

            $wheres[] = $SHOP_WHERE;
         }

          $SHOP_WHERE = implode(" AND ",$wheres);

          $SQL_FIELDS = "SQL_CALC_FOUND_ROWS id,COUNT(id) AS numMerchants,MIN(price) as minPrice, ROUND( (MIN(price)*100/price_old) ) as percent, price_old, color, size, gender, shipping_time";
        }
        break;
    }

    $offset = ($page-1) * $config_resultsPerPage;

    $SQL_SKIP = $offset;
    $SQL_TAKE = $config_resultsPerPage;

    $maxPercent = Product::max_percent($SHOP_WHERE.$FILTERS_WHERE);
    $products_max_price = Product::max_price($SHOP_WHERE,$where_by_attr);
    $products_min_price = Product::min_price($SHOP_WHERE,$where_by_attr);
    $rows = Product::selectRaw($SQL_FIELDS)->whereRaw($SHOP_WHERE.$FILTERS_WHERE)->groupBy($SQL_GROUP)->orderByRaw($SQL_ORDER)->skip($SQL_SKIP)->take($SQL_TAKE)->get()->toArray();

    //var_dump(end(DB::getQueryLog()));
    $numRows = count($rows);

    $rowsResultCount = DB::select(DB::raw("SELECT FOUND_ROWS() as resultcount"));
    $resultCount = $rowsResultCount[0]->resultcount;

    $banner["p_under_filter"] = $config_resultsComment;
    $banner["p_under_filter"] = str_replace("[Count]",$resultCount,$banner["p_under_filter"]);
    $banner["p_under_filter"] = str_replace("[Brand]",ucfirst(Input::get("catalog_by_brand")),$banner["p_under_filter"]);
    $banner["p_under_filter"] = str_replace("[Category]",ucfirst(Input::get("catalog_by_category")),$banner["p_under_filter"]);
    $banner["p_under_filter"] = str_replace("[MaxPercent]","{$maxPercent}%",$banner["p_under_filter"]);


    if ($resultCount) {
      $resultFrom = ($offset+1);

      $resultTo = ($offset+$config_resultsPerPage);

      if ($resultTo > $resultCount) $resultTo = $resultCount;

      $banner["under_filter"] .= "";

      $sortHREF = $config_baseHREF."search.php?q=".urlencode($q)."&amp;page=1&amp;";

      if ($minPrice || $maxPrice) {
        $sortHREF .= "minPrice=".$minPrice."&amp;maxPrice=".$maxPrice."&amp;";
      }

      foreach($FILTER_PARAMS_LIST as $param) {
        if($$param) {
          $sortHREF .= "&{$param}=".urlencode($$param)."&amp;";
        }
      }

      if ($config_useInteraction) {
        $sortRating = ", ".($sort=="rating"?"<strong>".translate("Product Rating")."</strong>":"<a href='".$sortHREF."rating'>".translate("Product Rating")."</a>");
      } else {
        $sortRating = "";
      }

      $sortPercent = ($sort=='percentAsc')?"<strong>%</strong>":"<a href={$sortHREF}>%</a>";
      $sortPrice = ($sort=='priceAsc')?"<strong>$</strong>":"<a href={$sortHREF}sort=priceAsc>$</a>";

      $banner["under_filter_2"] = "<span class=sorting>Sorieren nach: {$sortPercent} | {$sortPrice}</span>";

      $navigation["resultCount"] = $resultCount;

      $searchresults["numRows"] = $numRows;

      $ids = array();

      foreach($rows as $row) {
        $ids[] = $row["id"];
      }

      $in = implode(",",$ids);

      $sql2_fields = "id,name,normalised_name,image_url,description,price,rating";

      $rows2 = Product::selectRaw($sql2_fields)->whereRaw("id IN (".$in.")")->get()->toArray();

      $rows3 = array();

      foreach($rows2 as $row2) {
        $rows3[$row2["id"]] = $row2;
      }

      $searchresults["products"] = $rows;

      foreach($searchresults["products"] as $k => $product) {
        $searchresults["products"][$k] = array_merge($searchresults["products"][$k],$rows3[$product["id"]]);

        $searchresults["products"][$k]["productHREF"] = tapestry_productHREF($searchresults["products"][$k]);
      }
    } else {
      $banner["h2"] .= "(".translate("no results found").")";
    }

    $header["title"] = $q;
    if($_GET['catalog_by_category']) {
        $header["title"] .= ' '.$_GET['catalog_by_category'];
    }

    if ($page > 1) {
      $header["title"] .= " | ".translate("Page")." ".$page;
    }
  }


  switch($sort) {
    case "priceAsc":
      $header["title"] .= " | cheapest";
      break;
    case "percentAsc":
      $header["title"] .= " | biggest discount";
      break;
  }

  if($_GET['catalog_by_brand']) {
    $brand = $_GET['catalog_by_brand'];
    $product = Product::where("brand","=",$brand)->first();
    $SHOP_TITLE_IMG = $product['brand_logo'];
    $SHOP_TITLE_IMG_URL = "/b/".Input::get("catalog_by_brand")."/";
    $SHOP_TITLE = $brand." ".translate("SALE");
    $TOP_CATEGORIES = Product::top_categories_by_brand($brand);
  }

  if($_GET['catalog_by_category']) {
    $category = $_GET['catalog_by_category'];
    $SHOP_TITLE = $SHOP_TITLE? str_replace(translate("SALE"), $category, $SHOP_TITLE):$category." ".translate("SALE");
    $TOP_BRANDS = Product::top_brands_by_category($category);
  }

  if($searchresults["numRows"]==0) {
    if(Input::get("no_redirect")) {
      die("no results");
    }
    if(Input::get("catalog_by_brand") && Input::get("catalog_by_category")) {
      header("Location: /b/".Input::get("catalog_by_brand")."/",true,302);
      die();
    } 
    if(Input::get("catalog_by_brand") || Input::get("catalog_by_category")) {
      header("Location: /",true,302);
      die();
    }
  }

  require "search.autocomplete.php";
  require "html/header.php";
  require "html/menu.php";
  require "html/searchform.php";
  require "html/banner.php";

  if (isset($searchresults))
  {
    if ($searchresults["numRows"]) {
      require("html/searchfilters.php");
      require("html/search.underfilter.php");
      require("html/searchresults.php");
    } else {
      require("html/noresults.php");
    }
  }

  if (isset($navigation)) {
    if ($minPrice || $maxPrice) {
      $sort .= "&amp;minPrice=".$minPrice."&amp;maxPrice=".$maxPrice;
    }

    foreach($FILTER_PARAMS_LIST as $param) {
      if($$param) {
        $sort .= "&amp;{$param}=".urlencode($$param);
      }
    }
    require("html/navigation.php");
  }

  require("html/footer.php");
?>
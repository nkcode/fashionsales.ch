<?

function multiselect_input_tag($control_name, $rows, $attr_name, $selected, $base_rows) {

    if(isset($base_rows) && count($base_rows)>=1) {
      $got_options = " got_options ";
    } else {
      if(count($rows)==0) {
        $empty = " empty ";
      } else
      if(count($rows)==1) {
        $one_row = " one_row ";
      } else {
        $got_options = " got_options ";
      }
    }
    

    $size = "medium-2";
    print "<div class='small-12 {$size} columns'>";

    echo "<input name='".$control_name."' type=hidden value='".join(",",$selected)."'>";
    $placeholder = translate($attr_name);
    print "<select id=multiselect_".$control_name." class='multiselect ".$got_options.$one_row.$empty."' data-placeholder='".$placeholder."' data-target-input-name=".$control_name." id=".$control_name.">";

    foreach($selected as &$selected_item) {
      $selected_item = strtolower($selected_item);
    }
    foreach($rows as $name=>$count) {
      $val = Product::umlaute_fix($name);
      $selected_tag = in_array(strtolower($val),$selected)?"data-selected":"";
      print "<option value='".htmlspecialchars($val,ENT_QUOTES,$config_charset)."' ".$selected_tag.">".$val." (".$count.") </option>";
    }
    print "</select>";
    print "</div>";
}


function filter_string_to_array($list_string) {
    $list = explode(",",$list_string);
    foreach($categories_list as &$value) {
      $value = str_replace("-"," ",$value);
      $value = database_safe($value);
    }
    return $list;
}

function name_for_url($s) {
  return str_replace(" ","-",strtolower($s));
}

function char_fix($s) {
  return Product::umlaute_fix($s);
}
?>
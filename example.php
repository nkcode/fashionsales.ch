<?php
  require("includes/common.php");

  $header["title"] = "Example Page";

  $header["meta"]["description"] = "Example page meta description";

  $header["meta"]["keywords"] = "Example, Page, Meta, Keywords";

  require("html/header.php");
?>

<div class='row'>
  <p>This is an example page that you can copy to create additional pages on your site such as an About Us page that use the standard header and footer with customisable title and meta tags as required.</p>
</div>

<?php
  require("html/footer.php");
?>
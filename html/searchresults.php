<?php
if (file_exists("html/user_searchresults_before.php")) require("html/user_searchresults_before.php");
?>
<script type="text/javascript">
//    $("img.pt_sr_image").lazyload();
function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}
function restart(){
    setTimeout(function() {
//        $("img.pt_sr_image").trigger("sporty");
        $( "img.pt_sr_image" ).each(function( index ) {
            if(isScrolledIntoView(this) && $(this).data('image')!=$(this).attr('src')){
                $(this).lazyload({
                    data_attribute : "image",
                    placeholder: "/images/loading.gif"
                });
                if(location.search.indexOf("no_restart_log")==-1) {
                  console.log($(this).data('image'));
                  console.log($(this).attr('src'));
                  console.log('---');
                }
            }
        });
        restart();
    }, 50);
}
$(function() {
//    $("img.pt_sr_image").lazyload({
//        event : "sporty",
//        data_attribute : "image",
//        placeholder: "/images/loading.gif"
//    });

    $(window).bind("load", function() {
        console.log("window load")
        restart();
    });

});

</script>

<div class="main-wrapper">

  <ul class='pt_sr medium-block-grid-4'>

    <?php foreach($searchresults["products"] as $product): ?>

      <li class='row pt_sr_each'><a title="Zum Shop" target="_blank" href='<?php print tapestry_buyURL($product);/*print $product["productHREF"];*/ ?>'>

        <div class='small-2 columns show-for-small-only'>&nbsp;</div>

        <div class='small-8 medium-12 columns'>

          <div class='pt_sr_image_container'>
            
            <?php if ($product["image_url"]): ?>
              <div class="pt_sr_image_wrapper">
                <img alt='<?php print translate("Bild von"); ?> <?php print htmlspecialchars($product["name"],ENT_QUOTES,$config_charset); ?>' class='lazyItem pt_sr_image' data-image="<?php print htmlspecialchars($product["image_url"],ENT_QUOTES,$config_charset);?>" />
              </div>
            <?php else: ?>

              &nbsp;

            <?php endif; ?>
            
            <div class='pt_sr_image_div medium-12 columns'>

              <h4>

                <?php print $product["name"]; ?>

                <?php if ($config_useInteraction): ?>

                  <?php if ($product["rating"]) print tapestry_stars($product["rating"],""); ?>

                <?php endif; ?>

              </h4>
              
              <?php if((int)$product["price_old"]){ ?><div class='pt_sr_price economy'>-<?php print (100 - ceil(($product["minPrice"]*100)/$product["price_old"])); ?>%</div><?php } ?>
              
		  <!--<?php if((int)$product["price_old"]){ ?><div class='pt_sr_price old_price'><?php print tapestry_price($product["price_old"]); ?></div><?php } ?>
      <div class='pt_sr_price current_price'><?php print tapestry_price($product["minPrice"]); ?></div>-->

      <?php if ($product["description"] && false): ?>

        <p><?php print tapestry_substr($product["description"],250,"..."); ?></p>

      <?php endif; ?>
      <ul class="pt_sr_chars">
       <li>
        <span class="pt_sr_price old_price"><?php if((int)$product["price_old"]){ ?><?php print tapestry_price($product["price_old"]); ?><?php } ?></span>
      </li>

      <li class="wide">
        <span class="pt_sr_price current_price"><?php print tapestry_price($product["minPrice"]); ?></span>
      </li>
      <li>
        <strong><?php echo $translate["Color"]; ?>:</strong>
        <?php print $product["color"]; ?>
      </li>

      <li class="wide">
        <strong><?php echo $translate["Size"]; ?>:</strong>
        <?php print $product["size"]; ?>
      </li>

      <li>
        <strong>F&uuml;r:</strong>
        <?php print $product["gender"]; ?>
      </li>

      <li class="wide">
        <strong>Versand:</strong>
        <?php print $product["shipping_time"]? $product["shipping_time"]: "Gratis"; ?>
      </li>
    </ul>
    
  </div>

</div>

</div>

<div class='small-2 columns show-for-small-only'>&nbsp;</div>

</a>
</li>

<?php endforeach; ?>

</ul>

<?php
if (file_exists("html/user_searchresults_after.php")) require("html/user_searchresults_after.php");
?>
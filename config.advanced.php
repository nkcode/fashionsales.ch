<?php
  $config_adminPassword = "";

  $config_feedDirectory = "../feeds/";

  $config_databaseDebugMode = FALSE;

  $config_useFullText = TRUE;

  $config_searchDescription = FALSE;

  $config_fieldSet = array();
  $config_fieldSet["name"] = "Product Name";
  $config_fieldSet["description"] = "Product Description";
  $config_fieldSet["image_url"] = "Image URL";
  $config_fieldSet["buy_url"] = "Buy URL";
  $config_fieldSet["price"] = "Price";
  $config_fieldSet["category"] = "Category";
  $config_fieldSet["brand"] = "Brand";
  //i need this field to add: Price_old, ShippingTime, color, size, gender, ImgUrl2, ImgUrl3, ImgUrl4
  $config_fieldSet["price_old"] = "Old Price";
  $config_fieldSet["shipping_time"] = "Shipping Time";
  $config_fieldSet["color"] = "Color";
  $config_fieldSet["size"] = "Size";
  $config_fieldSet["gender"] = "Gender";
  $config_fieldSet["ImgUrl2"] = "Image Url 2";
  $config_fieldSet["ImgUrl3"] = "Image Url 3";
  $config_fieldSet["ImgUrl4"] = "Image Url 4";
  $config_fieldSet["brand_logo"] = "Brand Logo";

  $config_brandTitle = "SALE [Brand] [Category] [MaxPercent]% reduziert kaufen - FashionSales.ch";
  $config_brandDescription = "[Brand] [Category] SALE Shop mit bis zu [MaxPercent]% Rabatt. Alle [Category] von [Brand] Aktionen finden Sie hier - fashionsales.ch";

  $config_categoryTitle = "[Category] im SALE bis zu [MaxPercent]% reduziert kaufen - FashionSales.ch";
  $config_categoryDescription = "[Category] im SALE bis zu [MaxPercent]% reduziert. Alle [Category] Aktionen der Schweiz an einem Ort - fashionsales.ch";
  $config_resultsComment = "SALE! Aktuell haben wir [Count] [Brand] [Category] Artikel bis [MaxPercent] reduziert.";

  $config_commonFields = array();
  $config_commonFields["name"] =
    array("TEXT/NAME","PRODUCT-NAME","product title","PRODUCT TITLE","Product Title","product_title","PRODUCT_TITLE","Product_Title","PRODUCT NAME","product name","Product Name","PRODUCT_NAME","product_name","Product_Name","producttitle","PRODUCTTITLE","ProductTitle","productTitle","productname","PRODUCTNAME","ProductName","productName","name","NAME","Name");
  $config_commonFields["description"] =
    array("TEXT/DESC","DESCRIPTION/LONG","DESCRIPTION/SHORT","long description","LONG DESCRIPTION","Long Description","long_description","LONG_DESCRIPTION","Long_Description","longdescription","LONGDESCRIPTION","LongDescription","longDescription","short description","SHORT DESCRIPTION","Short Description","short_description","SHORT_DESCRIPTION","Short_Description","shortdescription","SHORTDESCRIPTION","ShortDescription","shortDescription","description","DESCRIPTION","Description");
  $config_commonFields["image_url"] =
    array("URL/PRODUCTIMAGE","URI/MIMAGE","URI/AWIMAGE","image url","IMAGE URL","Image Url","Image URL","image_url","IMAGE_URL","Image_Url","Image_URL","imageurl","IMAGEURL","ImageUrl","ImageURL","imageUrl","imageURL","image","IMAGE","Image");
  $config_commonFields["buy_url"] =
    array("URI/AWTRACK","URL/PRODUCT","product link","PRODUCT LINK","Product Link","product_link","PRODUCT_LINK","Product_Link","productlink","PRODUCTLINK","ProductLink","productLink","producturl","PRODUCTURL","ProductUrl","productUrl","deep link","DEEP LINK","Deep Link","deep_link","DEEP_LINK","Deep_Link","deeplink","DEEPLINK","DeepLink","deepLink","buy url","BUY URL","Buy Url","buy URL","Buy URL","buy url","BUY_URL","Buy_Url","buy_URL","Buy_URL","buyurl","BUYURL","BuyUrl","buyURL","buyURL","BuyURL","url","URL","Url","aw_deep_link","buyat_short_deeplink_url");
  $config_commonFields["price"] =
    array("PRICE/PRICE","PRICE/BUYNOW","price","PRICE","Price","search_price","online_price","our_price");
  $config_commonFields["category"] =
    array("CAT/AWCAT","CATEGORY/PRIMARY","mastercategory","MASTERCATEGORY","MasterCategory","masterCategory","merchant category","MERCHANT CATEGORY","Merchant Category","merchant_category","MERCHANT_CATEGORY","Merchant_Category","category_name","CATEGORY_NAME","Category_Name","category name","CATEGORY NAME","Category Name","category","CATEGORY","Category","TDCategoryName","ADVERTISERCATEGORY");
  $config_commonFields["brand"] =
    array("BRAND/BRANDNAME","merchant brand","MERCHANT BRAND","Merchant Brand","merchant_brand","MERCHANT_BRAND","Merchant_Brand","brand_name","BRAND_NAME","Brand_Name","brand name","BRAND NAME","Brand Name","brandname","BRANDNAME","BrandName","brandName","BRAND","brand","Brand","MANUFACTURER");

  $config_slowImportBlock = 500;

  $config_slowImportSleep = 0;

  $config_useVoucherCodes = 2;

  $config_automationHandler = "auto";

  $config_automationUnzipPrograms = array();
  $config_automationUnzipPrograms["unzip"] = "/usr/bin/unzip";
  $config_automationUnzipPrograms["gzip"] = "/bin/gzip";

  $config_CRONPrograms = array();
  $config_CRONPrograms["php"] = array("/usr/bin/php","/usr/local/bin/php");
  $config_CRONPrograms["wget"] = array("/usr/bin/wget","/usr/local/bin/wget");

  $config_nicheMode = FALSE;

  $config_enableSearchFilters = TRUE;

  $config_voucherCodesFeedDirectory = "../voucherfeeds/";

  $config_voucherCodesFieldSet = array();
  $config_voucherCodesFieldSet["merchant"] = "Merchant Name";
  $config_voucherCodesFieldSet["code"] = "Voucher Code";
  $config_voucherCodesFieldSet["valid_from"] = "Valid From";
  $config_voucherCodesFieldSet["valid_to"] = "Valid To";
  $config_voucherCodesFieldSet["description"] = "Description";

  $config_voucherCodesCommonFields = array();
  $config_voucherCodesCommonFields["merchant"] =
    array("merchant","program_name");
  $config_voucherCodesCommonFields["code"] =
    array("code");
  $config_voucherCodesCommonFields["valid_from"] =
    array("start_date");
  $config_voucherCodesCommonFields["valid_to"] =
    array("end_date");
  $config_voucherCodesCommonFields["description"] =
    array("description");

  $config_voucherCodesDiscountValuePrefixRegexp = "(save)";

  $config_voucherCodesDiscountValuePostfixRegexp = "(discount|off|voucher)";

  $config_voucherCodesMinSpendPrefixRegexp = "(spend|over|more than|at least)";

  $config_timezone = "UTC";
?>
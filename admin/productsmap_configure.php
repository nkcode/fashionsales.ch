<?php
  require("../includes/common.php");

  require("../includes/widget.php");

  $admin_checkPassword = TRUE;

  require("../includes/admin.php");

  $id = $_GET["id"];

  $sql = "SELECT * FROM `".$config_databaseTablePrefix."productsmap` WHERE id='".database_safe($id)."'";

  database_querySelect($sql,$rows);

  $productmap = $rows[0];

  $submit = (isset($_POST["submit"])?$_POST["submit"]:"");

  if ($submit == "Cancel")
  {
    header("Location: productsmap.php");

    exit();
  }

  if ($submit == "Save")
  {
    if (!isset($_POST["alternates"])) $_POST["alternates"] = "";

    if (!isset($_POST["description"])) $_POST["description"] = "";

    if (!isset($_POST["category"])) $_POST["category"] = "";

    if (!isset($_POST["brand"])) $_POST["brand"] = "";

    if (!isset($_POST["image_url"])) $_POST["image_url"] = "";

    if ($_POST["alternates"])
    {
      $uniqueAlternates1 = array();

      $alternates = explode("\n",widget_posted($_POST["alternates"]));

      foreach($alternates as $alternate)
      {
        $uniqueAlternates1[trim($alternate)] = 1;
      }

      $uniqueAlternates2 = array();

      foreach($uniqueAlternates1 as $alternate => $v)
      {
        $uniqueAlternates2[] = $alternate;
      }

      asort($uniqueAlternates2);

      $alternates = implode("\n",$uniqueAlternates2);
    }
    else
    {
      $alternates = "";
    }

    $sql = "UPDATE `".$config_databaseTablePrefix."productsmap` SET

              alternates  = '".database_safe($alternates)."',
              description = '".database_safe(widget_posted($_POST["description"]))."',
              category    = '".database_safe(widget_posted($_POST["category"]))."',
              brand       = '".database_safe(widget_posted($_POST["brand"]))."',
              image_url   = '".database_safe(widget_posted($_POST["image_url"]))."'

              WHERE id ='".database_safe($id)."'";

    database_queryModify($sql,$insertId);

    header("Location: productsmap.php");

    exit();
  }

  require("admin_header.php");

  print "<h2>".translate("Product Mapping")."</h2>";

  print "<h3>".translate("Configure")." (".$productmap["name"].")</h3>";

  print "<div class='row'>";

  print "<div class='small-6 columns'>";

  widget_formBegin();

  widget_textArea("Alternatives","alternates",FALSE,$productmap["alternates"],200,12);

  widget_textArea("Custom Description","description",FALSE,$productmap["description"],100,12);

  widget_textBox("Custom Category","category",FALSE,$productmap["category"],"",6);

  widget_textBox("Custom Brand","brand",FALSE,$productmap["brand"],"",6);

  widget_textBox("Custom Image URL","image_url",FALSE,$productmap["image_url"],"",6);

  widget_formButtons(array("Save"=>TRUE),TRUE);

  widget_formEnd();

  print "</div>";

  print "<div class='small-6 columns'>";

  $helper["field"] = "name";

  $helper["callbackKeyword"] = "callbackKeyword";

  $helper["callbackExact"] = "callbackExact";

  require("helper.php");

  print "</div>";
?>
  <script type='text/JavaScript'>

  function callbackKeyword(name)
  {
    alternates = document.getElementById("alternates");

    alternates.value = alternates.value + "\n" + name;
  }

  function callbackExact(name)
  {
    alternates = document.getElementById("alternates");

    alternates.value = alternates.value + "\n=" + name;
  }

  </script>
<?php
  require("admin_footer.php");
?>
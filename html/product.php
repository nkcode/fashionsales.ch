<?php
  $product_main = $product["products"][0];

  $product_bestPriceText = (count($product["products"])>1?translate("Best Price"):translate("Price"));

  $product_bestPriceMerchants = array();

  foreach($product["products"] as $p)
  {
    if ($p["price"] == $product_main["price"])
    {
      $html = "<a href='".tapestry_buyURL($p)."'>".$p["merchant"]."</a>";

      if ($p["voucher_code"])
      {
        $html .= " (".translate("using voucher code")." <span class='pt_pr_vouchercode'>".$p["voucher_code"]."</span>)";
      }

      $product_bestPriceMerchants[] = $html;
    }
  }

  $product_bestPriceMerchants = implode(", ",$product_bestPriceMerchants);
?>

<div class='row pt_p'>

  <?php if ($product_main["image_url"]): ?>

    <div class='small-2 columns show-for-small-only'>&nbsp;</div>

    <div class='small-8 medium-4 columns'><img alt='<?php print translate("Image of"); ?> <?php print htmlspecialchars($product_main["name"],ENT_QUOTES,$config_charset); ?>' src='<?php print htmlspecialchars($product_main["image_url"],ENT_QUOTES,$config_charset); ?>' /></div>

    <div class='small-2 columns show-for-small-only'>&nbsp;</div>

  <?php endif; ?>

  <div class='medium-8 columns'>

    <h1><?php print $product_main["name"]; ?></h1>

    <?php if ($product_main["description"]): ?>

      <p><?php print $product_main["description"]; ?></p>

    <?php endif; ?>

    <p>

      <strong><?php print $product_bestPriceText; ?>:</strong>

      <?php print tapestry_price($product_main["price"]); ?> <?php print translate("from"); ?>

      <?php print $product_bestPriceMerchants; ?>

    </p>

    <?php if (isset($product_main["extraHTML"])) print $product_main["extraHTML"]; ?>

  </div>

</div>
<?php
  require("../includes/common.php");

  require("../includes/MagicParser.php");

  $admin_checkPassword = TRUE;

  require("../includes/admin.php");

  require("../includes/widget.php");

  function database_toolPack($text)
  {
    return bin2hex($text);
  }

  function database_toolUnpack($hex)
  {
    return pack("H*",trim($hex));
  }

  // feeds
  $fields["feeds"][] = "filename";
  $fields["feeds"][] = "registered";
  $fields["feeds"][] = "format";
  $fields["feeds"][] = "merchant";
  $fields["feeds"][] = "user_category";
  $fields["feeds"][] = "user_brand";
  $fields["feeds"][] = "field_merchant";
  foreach($config_fieldSet as $field => $v)
  {
    $fields["feeds"][] = "field_".$field;
  }

  // filters
  $fields["filters"][] = "filename";
  $fields["filters"][] = "field";
  $fields["filters"][] = "name";
  $fields["filters"][] = "created";
  $fields["filters"][] = "data";

  // categories
  $fields["categories"][] = "name";
  $fields["categories"][] = "alternates";

  // productsmap
  $fields["productsmap"][] = "name";
  $fields["productsmap"][] = "alternates";
  $fields["productsmap"][] = "category";
  $fields["productsmap"][] = "brand";
  $fields["productsmap"][] = "image_url";
  $fields["productsmap"][] = "description";

  // featured
  $fields["featured"][] = "name";
  $fields["featured"][] = "sequence";

  // vouchers
  $fields["vouchers"][] = "merchant";
  $fields["vouchers"][] = "code";
  $fields["vouchers"][] = "match_type";
  $fields["vouchers"][] = "match_field";
  $fields["vouchers"][] = "match_value";
  $fields["vouchers"][] = "discount_type";
  $fields["vouchers"][] = "discount_value";
  $fields["vouchers"][] = "discount_text";
  $fields["vouchers"][] = "min_price";
  $fields["vouchers"][] = "valid_from";
  $fields["vouchers"][] = "valid_to";

  // jobs
  $fields["jobs"][] = "directory";
  $fields["jobs"][] = "filename";
  $fields["jobs"][] = "url";
  $fields["jobs"][] = "unzip";
  $fields["jobs"][] = "minsize";
  $fields["jobs"][] = "lastrun";
  $fields["jobs"][] = "status";

  // voucherfeeds
  $fields["voucherfeeds"][] = "filename";
  $fields["voucherfeeds"][] = "registered";
  $fields["voucherfeeds"][] = "format";
  $fields["voucherfeeds"][] = "field_merchant";
  $fields["voucherfeeds"][] = "field_code";
  $fields["voucherfeeds"][] = "field_valid_from";
  $fields["voucherfeeds"][] = "field_valid_to";
  $fields["voucherfeeds"][] = "field_description";
  $fields["voucherfeeds"][] = "merchant_mappings";

  function database_toolDumpRecord($table,$record)
  {
    global $fields;

    $xml = "";

    $xml .= "<".strtoupper($table)."_R>\n";

    foreach($fields[$table] as $field)
    {
      $xml .= "<".strtoupper($field).">".database_toolPack($record[$field])."</".strtoupper($field).">\n";
    }

    $xml .= "</".strtoupper($table)."_R>\n";

    return $xml;
  }

  function database_toolRestoreRecord($table,$record)
  {
    global $fields;

    global $config_databaseTablePrefix;

    $sql = "INSERT INTO `".$config_databaseTablePrefix.$table."` SET ";

    foreach($fields[$table] as $field)
    {
      if (isset($record[strtoupper($field)]))
      {
        $sql .= $field." = '".database_safe(database_toolUnpack($record[strtoupper($field)]))."',";
      }
    }

    $sql = trim($sql,",");

    database_queryModify($sql,$result);
  }

  function database_toolDumpTable($table)
  {
    global $config_databaseTablePrefix;

    $sql = "SELECT * FROM `".$config_databaseTablePrefix.$table."`";

    $xml = "";

    if (database_querySelect($sql,$records))
    {
      $xml = "<".strtoupper($table).">\n";

      foreach($records as $record)
      {
        $xml .= database_toolDumpRecord($table,$record);
      }

      $xml .= "</".strtoupper($table).">\n";
    }

    return $xml;
  }

  if (!isset($_POST["action"])) $_POST["action"] = "";

  if ($_POST["action"] == "backup")
  {
    $xml = "<BACKUP>\n";

    if (isset($_POST["feeds"]))
    {
      $xml .= database_toolDumpTable("jobs");

      $xml .= database_toolDumpTable("feeds");

      $xml .= database_toolDumpTable("filters");
    }

    if (isset($_POST["vouchers"]))
    {
      $xml .= database_toolDumpTable("vouchers");

      $xml .= database_toolDumpTable("voucherfeeds");
    }

    if (isset($_POST["featured"]))
    {
      $xml .= database_toolDumpTable("featured");
    }

    if (isset($_POST["categories"]))
    {
      $xml .= database_toolDumpTable("categories");
    }

    if (isset($_POST["productsmap"]))
    {
      $xml .= database_toolDumpTable("productsmap");
    }

    $xml .= "</BACKUP>";

    header("Content-Type: application/octet-stream");

    header("Content-Disposition: attachment; filename=Backup".date("Ymd").".xml");

    print $xml;

    exit();
  }

  function myRecordHandler($record)
  {
    global $config_databaseTablePrefix;

    global $table;

    global $first;

    if ($first)
    {
      $sql = "TRUNCATE `".$config_databaseTablePrefix.$table."`";

      database_queryModify($sql,$result);

      if ($table == "feeds")
      {
        $sql = "TRUNCATE `".$config_databaseTablePrefix."filters`";

        database_queryModify($sql,$result);
      }
    }

    database_toolRestoreRecord($table,$record);

    $first = FALSE;
  }

  if ($_POST["action"] == "restore")
  {
    if (is_uploaded_file($_FILES['backup']['tmp_name']))
    {
      $xml = file_get_contents($_FILES['backup']['tmp_name']);

      $table = "feeds";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/FEEDS/FEEDS_R/");

      $table = "filters";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/FILTERS/FILTERS_R/");

      $table = "categories";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/CATEGORIES/CATEGORIES_R/");

      $table = "productsmap";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/PRODUCTSMAP/PRODUCTSMAP_R/");

      $table = "featured";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/FEATURED/FEATURED_R/");

      $table = "vouchers";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/VOUCHERS/VOUCHERS_R/");

      $table = "jobs";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/JOBS/JOBS_R/");

      $table = "voucherfeeds";

      $first = TRUE;

      MagicParser_parse("string://".$xml,"myRecordHandler","xml|BACKUP/VOUCHERFEEDS/VOUCHERFEEDS_R/");

      header("Location: ".$config_baseHREF."admin/");

      exit();
    }
    else
    {
      widget_errorSet("backup","file upload failed");
    }
  }

  require("admin_header.php");

  print "<h2>".translate("Database Tool")."</h2>";

  print "<div class='row'>";

  print "<div class='medium-6 columns'>";

  print "<h3>".translate("Backup")."</h3>";

  widget_formBegin();

  widget_checkBox("Automation Tool Jobs, Feed Registration, Filters and Global Filters","feeds",FALSE,TRUE,12);

  widget_checkBox("Voucher Codes and Voucher Code Feed Registration","vouchers",FALSE,TRUE,12);

  widget_checkBox("Featured Products","featured",FALSE,TRUE,12);

  widget_checkBox("Category Mapping","categories",FALSE,TRUE,12);

  widget_checkBox("Product Mapping","productsmap",FALSE,TRUE,12);

  widget_formHidden("action","backup");

  widget_formButtons(array("Backup"=>TRUE));

  widget_formEnd();

  print "</div>";

  print "<div class='medium-6 columns'>";

  print "<h3>".translate("Restore")."</h3>";

  print "<form method='post' enctype='multipart/form-data'>";

  widget_file("Backup Filename","backup",TRUE);

  widget_formHidden("action","restore");

  widget_formButtons(array("Restore"=>TRUE));

  print "</form>";

  print "</div>";

  print "</div>";

  require("admin_footer.php");
?>
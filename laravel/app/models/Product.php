<?

//converting products table to utf8
/*

create table pt_products_backup like pt_products;
insert pt_products_backup select * from pt_products;
ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE category category VARBINARY(255);
ALTER TABLE pt_products CHANGE category category VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;


ALTER TABLE `pt_products` DROP KEY `name_2`;
ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE name name VARBINARY(255);
ALTER TABLE pt_products CHANGE name name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `pt_products` ADD FULLTEXT KEY `name_2` (`name`);


ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE normalised_name normalised_name VARBINARY(255);
ALTER TABLE pt_products CHANGE normalised_name normalised_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE merchant merchant VARBINARY(32);
ALTER TABLE pt_products CHANGE merchant merchant VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE brand brand VARBINARY(32);
ALTER TABLE pt_products CHANGE brand brand VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;


ALTER TABLE pt_products CHANGE color color VARCHAR(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE color color VARBINARY(64);
ALTER TABLE pt_products CHANGE color color VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;


ALTER TABLE pt_products CHANGE size size VARCHAR(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE size size VARBINARY(64);
ALTER TABLE pt_products CHANGE size size VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE pt_products CHANGE gender gender VARCHAR(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE gender gender VARBINARY(64);
ALTER TABLE pt_products CHANGE gender gender VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

*/

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Product extends Eloquent {
	
  protected $table = 'pt_products';
  public $timestamps = false;


  public function color_fix_for_enums($color_count) {
    $ret = array();

    foreach($color_count as $color=>$count) {
      $count = intval($count);
      $colors = array($color);
      if(strpos($color,"/")!==false) {
        $colors = split("/",$color);
      }
      if(strpos($color,",")!==false) {
        $colors = split(",",$color);
      }
      foreach($colors as $c) {
        $c = trim(strtolower($c));
        if(isset($ret[$c])) {
          $ret[$c] = $ret[$c] + $count;
        } else {
          $ret[$c] = $count;
        }
      }

    }

    return $ret;


  }

  public function field_charset_set($field_name,$size,$charset,$collate) {
    DB::statement(
      "ALTER TABLE pt_products CHANGE {$field_name} {$field_name} VARCHAR({$size}) CHARACTER SET {$charset} COLLATE {$collate} NOT NULL");
  }

  public function field_force_to_utf8($field_name,$size) {
      DB::statement(
        "ALTER TABLE pt_products CHARACTER SET utf8 COLLATE utf8_unicode_ci, CHANGE {$field_name} {$field_name} VARBINARY({$size});");
      Product::field_charset_set($field_name,$size,"utf8","utf8_unicode_ci");
  }

  public function field_fix_after_import($field_name,$size) {
    echo "fixing field: {$field_name}<br>";flush();ob_flush();
    echo "field: {$field_name}, marking as latin charset<br>";flush();ob_flush();
    Product::field_charset_set($field_name,$size,"latin1","latin1_swedish_ci");
    echo "field: {$field_name}, converting to utf8 <br>";flush();ob_flush();
    Product::field_force_to_utf8($field_name,$size);
    echo "done<br>";flush();ob_flush();
    echo_ln("");
  }

  public function where_remove_key_and_to_s($where_by_attr,$remove_key) {
    $wheres = array();
    $wheres[] = "true";
    foreach($where_by_attr as $k=>&$v) {
      //$v = utf8_encode($v);
      if($k!=$remove_key) {
        $wheres[] = $v;
      }
    }

    $where_filtered = join(" AND ",$wheres);
    return $where_filtered;

  }

  public function brand_logo($brand) {
    $brand = Product::umlaute_restore($brand);

    $product = Product::where("brand","=",$brand)->first();
    return $product['brand_logo'];
  }


  public function top_categories_by_brand($brand) {
        $brand = Product::umlaute_restore($brand);

        $filtered_count = Product::selectRaw("category, count(1) as count")
          ->where("brand","=",$brand)->where("category","<>","")
          ->orderBy("count","desc")->limit(10)->groupBy("category")->get()->toArray();

        $hash = array();        
        foreach($filtered_count as $entry) $hash[char_fix($entry["category"])] = $entry['count'];
        return $hash;

  }


  public function max_percent($where) {
    $maxPercent = Product::selectRaw("MAX((price_old-price)*100/price_old)")->whereRaw($where)->get()->toArray();    
    $maxPercent = array_values($maxPercent[0]);
    $maxPercent = floor($maxPercent[0]);
    return $maxPercent;

  }


  public function price_query($expr, $shop_where, $where_by_attr) {
    $where_by_attr['shop'] = $shop_where;

    $where_filtered = Product::where_remove_key_and_to_s($where_by_attr,"price");


    $ret = Product::selectRaw($expr)->whereRaw($where_filtered)->get()->toArray();    
    $ret = array_values($ret[0]);
    $ret = floor($ret[0]);
    return $ret;
  }

  public function max_price($shop_where, $where_by_attr) {
    return Product::price_query("MAX(price)", $shop_where, $where_by_attr);
  }

  public function min_price($shop_where, $where_by_attr) {
    return Product::price_query("MIN(price)", $shop_where, $where_by_attr);
  }

  public function umlaute_hash() {
    return array( 'Ã¼'=>'ü', 'Ã¤'=>'ä', 'Ã¶'=>'ö', 'Ã–'=>'Ö', 'ÃŸ'=>'ß', 'Ã '=>'à', 'Ã¡'=>'á', 'Ã¢'=>'â', 'Ã£'=>'ã', 'Ã¹'=>'ù', 'Ãº'=>'ú', 'Ã»'=>'û', 'Ã™'=>'Ù', 'Ãš'=>'Ú', 'Ã›'=>'Û', 'Ãœ'=>'Ü', 'Ã²'=>'ò', 'Ã³'=>'ó', 'Ã´'=>'ô', 'Ã¨'=>'è', 'Ã©'=>'é', 'Ãª'=>'ê', 'Ã«'=>'ë', 'Ã€'=>'À', 'Ã'=>'Á', 'Ã‚'=>'Â', 'Ãƒ'=>'Ã', 'Ã„'=>'Ä', 'Ã…'=>'Å', 'Ã‡'=>'Ç', 'Ãˆ'=>'È', 'Ã‰'=>'É', 'ÃŠ'=>'Ê', 'Ã‹'=>'Ë', 'ÃŒ'=>'Ì', 'Ã'=>'Í', 'ÃŽ'=>'Î', 'Ã'=>'Ï', 'Ã‘'=>'Ñ', 'Ã’'=>'Ò', 'Ã“'=>'Ó', 'Ã”'=>'Ô', 'Ã•'=>'Õ', 'Ã˜'=>'Ø', 'Ã¥'=>'å', 'Ã¦'=>'æ', 'Ã§'=>'ç', 'Ã¬'=>'ì', 'Ã­'=>'í', 'Ã®'=>'î', 'Ã¯'=>'ï', 'Ã°'=>'ð', 'Ã±'=>'ñ', 'Ãµ'=>'õ', 'Ã¸'=>'ø', 'Ã½'=>'ý', 'Ã¿'=>'ÿ', 'â‚¬'=>'€', 'Â´'=>'´' );
  }
  public function umlaute_fix($s) {
    //from here: http://www.sebastianviereck.de/mysql-php-umlaute-sonderzeichen-utf8-iso/
    $umlaute = Product::umlaute_hash();
    foreach ($umlaute as $key => $value) {
      $s = str_replace($key,$value,$s);
    }
    return $s;
  }

  public function umlaute_restore($s) {
    $umlaute = Product::umlaute_hash();
    foreach ($umlaute as $key => $value) {
      $s = str_replace($value,$key,$s);
    }
    return $s;
  }



  public function top_brands_by_category($category) {
        $filtered_count = Product::selectRaw("brand, count(1) as count")
          ->where("category","=",$category)->where("brand","<>","")
          ->orderBy("count","desc")->limit(10)->groupBy("brand")->get()->toArray();

        $hash = array();        
        foreach($filtered_count as $entry) $hash[char_fix($entry["brand"])] = $entry['count'];
        return $hash;
  }

  public function count_by_attribute($attr_name, $where_all, $where_by_attr) {
        $where_by_attr["all"] = $where_all;
        $where_filtered = Product::where_remove_key_and_to_s($where_by_attr,$attr_name);

        $filtered_count = Product::selectRaw($attr_name.",count(1) as count")
          ->whereRaw($where_filtered)
          ->whereRaw($attr_name." <> ''")
          ->orderBy($attr_name)->groupBy($attr_name)->get()->toArray();


        $hash = array();        
        foreach($filtered_count as $entry) $hash[$entry[$attr_name]] = $entry['count'];
       	return $hash;
	}

  public function count_by_sizes($where_all, $where_by_attr) {
        $where_by_attr["all"] = $where_all;
        $where_filtered = Product::where_remove_key_and_to_s($where_by_attr,'size');


        $filtered_count = ProductSize::selectRaw("size_extracted, count(pt_products.id) as count")    
          ->join("pt_products","pt_product_sizes.product_id","=","pt_products.id")
          ->whereRaw($where_filtered)
          ->orderBy("size_extracted")
          ->groupBy("size_extracted")
          ->get()->toArray();


        $hash = array();        
        foreach($filtered_count as $entry) $hash[$entry['size_extracted']] = $entry['count'];
        return $hash;
  }

  public function sizes() {
    return $this->hasMany("ProductSize");
  }

  public function build_sizes() {
    if($this->sizes) {
      $this->sizes()->delete();
    }

    $sizes = explode("|",$this->size);

    foreach($sizes as $size) {
      $size_rec = new ProductSize;
      $size_rec->product_id = $this->id;
      $size_rec->size_extracted = $size;
      $size_rec->save();
    }
  }

}

?>
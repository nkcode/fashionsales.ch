<?php
  require("includes/common.php");

  $header["title"] = "Impressum - Kontakt - FashionSales CH";

  $header["meta"]["description"] = "Kontaktformular und Impressum der Website fashionsales.ch";

  require("html/header.php");
  
  require("html/menu.php");
  
  require("html/searchform-home.php");
?>

<div class="main-wrapper-contact">

<div class="row">

<div class='contact-form small-11 medium-12 columns'>

   <h3>Kontakt</h3>

   <form name="" action="" method="post">
   
   <div class="small-12 medium-8 columns">
      <label>Ihr Name (Pflichtfeld)</label>
      <input type="text" name="your-name" value="" size="40">
   </div>  
	
   <div class="small-12 medium-8 columns">	
	  <label>Ihre E-Mail-Adresse (Pflichtfeld)</label>
      <input type="email" name="your-email" value="" size="40">
   </div>

   <div class="small-12 medium-8 columns">
      <label>Betreff</label>
      <input type="text" name="your-subject" value="" size="40">
   </div>
   
   <div class="small-12 medium-12 columns">
      <label>Ihre Nachricht</label>
      <textarea name="your-message" cols="40" rows="10" ></textarea>
   </div>
   
   <div class="small-4 medium-2 columns left">
      <input type="submit" value="Senden" class="button tiny form-submit">
   </div>
   
   </form>
 
</div>


<p>Die Inhalte der Website FashionSales unterliegen &ndash; sofern nicht anders gekennzeichnet &ndash; dem Urheberrecht und dürfen nicht ohne vorherige schriftliche Zustimmung von FashionSales weder als Ganzes noch in Teilen verbreitet, verändert oder kopiert werden. Die auf dieser Website eingebundenen Bilder dürfen nicht ohne vorherige schriftliche Zustimmung von FashionSales verwendet werden. Auf den Websites enthaltene Bilder unterliegen teilweise dem Urheberrecht Dritter.</p>
<h1>Haftungsausschluss</h1>

<p>Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluß zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind!</p>


<h1>Impressum</h1>
<p></p><div class="clear"></div>
<div><p><b>Jelena Mihajlovic</b> & <b>Camilla Assenza</b> - Bärematt 1 - 6017 Ruswil - Schweiz - Tel: +41 76 77 99 820</p></div>


</div>

<?php
  require("html/footer.php");
?>
<?php
  $atoz["itemsByLetter"] = array();

  foreach($atoz["items"] as $item)
  {
    $atoz_letter = tapestry_mb_strtoupper(tapestry_mb_substr($item["name"],0,1));

    $atoz["itemsByLetter"][$atoz_letter][] = $item;
  }
?>

<div class='row pt_az <?= $atoz_class ?>'>

  <div class='small-12 columns'>

    <?php foreach($atoz["itemsByLetter"] as $atoz_letter => $atoz_items): ?>

      <h4><?php print $atoz_letter; ?></h4>

      <ul class="atoz items small-block-grid-1 medium-block-grid-2 large-block-grid-4">

        <?php foreach($atoz_items as $atoz_item): ?>

          <li>


              <?php if (isset($atoz_item["logo"]) && $atoz_item["logo"]!=''): ?>

                <div class=item-with-logo>
                  <a href='<?php print strtolower($atoz_item["href"]); ?>'>
                    <img class="items_image" alt='<?php print htmlspecialchars($atoz_item["name"],ENT_QUOTES,$config_charset); ?>' data-image='<?php print $atoz_item["logo"]; ?>' /><br>
                    <span class=item-title>
                      <?php print $atoz_item["name"]; ?>
                      <span class=count>
                        <?php echo ' ( '.$atoz_item['count'].' )';?>
                      </span>

                    </span>
                  </a>
                </div>


              <?php else: ?>
                <div class=item-without-logo>
                  <a href='<?php print strtolower($atoz_item["href"]); ?>'>
                  <div class=no-image></div>
                  <span class=item-title>
                    <?php print $atoz_item["name"]; ?>
                    <span class=count><?php echo ' ( '.$atoz_item['count'].' )';?></span>
                  </span>
                  </a>
                </div>

              <?php endif; ?>

              

          </li>

        <?php endforeach; ?>

      </ul>

    <?php endforeach; ?>
      <script type="text/javascript">
          $(function() {
              $("img.items_image").lazyload({
                  event : "sporty-items",
                  data_attribute : "image"
              });
          });

          $(window).bind("load", function() {
              var timeout = setTimeout(function() { $("img.items_image").trigger("sporty-items") }, 1);
          });
      </script>

  </div>

</div>
